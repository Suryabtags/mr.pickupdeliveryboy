package in.mrpickup.deliveryboy;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import in.mrpickup.deliveryboy.appcontroller.Api;
import in.mrpickup.deliveryboy.appcontroller.AppController;
import in.mrpickup.deliveryboy.appcontroller.MyActivity;
import in.mrpickup.deliveryboy.appcontroller.PreferenceManager;
import in.mrpickup.deliveryboy.helper.Macaddress;
import in.mrpickup.deliveryboy.helper.NetworkError;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Signin extends AppCompatActivity implements View.OnClickListener {
    String uname, pass;
    private TextInputEditText username, password;
    private TextInputLayout lEmail, lPass;
    private Button signInBtn;
    private Context mContext;
    private PreferenceManager prefmanager;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        initObjects();
        initCall();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    private void getValues() {
        uname = username.getText().toString().trim();
        pass = password.getText().toString().trim();
    }

    private void initObjects() {
        username = findViewById(R.id.input_email1);
        password = findViewById(R.id.input_password);
        lEmail = findViewById(R.id.layout_phone);
        lPass = findViewById(R.id.layout_password);
        signInBtn = findViewById(R.id.signIn);
        mContext = this;
        prefmanager = new PreferenceManager(this);
        mProgressDialog = new ProgressDialog(this);
    }

    private void initCall() {
        lEmail.setOnClickListener(this);
        lPass.setOnClickListener(this);
        signInBtn.setOnClickListener(this);
    }

    private void login(JSONObject signInRequestJson) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Api.Loginurl,
                signInRequestJson, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                hideProgressDialog();
                handleLoginResponse(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();

                VolleyErrorHandler.handle(mContext, error);
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "login");
    }

    private void handleLoginResponse(JSONObject response) {
        String username, profilepic, token, user_id;
        try {
            username = response.getString(Api.key_username);
            token = response.getString(Api.key_token);
            user_id = response.getString(Api.key_userid);
            prefmanager.setToken(token);
            prefmanager.setName(username);
            MyActivity.launchClearStack(mContext, Orders.class);
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("erfyf", "" + e.toString());
        }
    }

    private JSONObject goLogin() {
        JSONObject logi = new JSONObject();
        try {
            logi.put(Api.key_client, Macaddress.getMacAddress());
            logi.put(Api.key_username, uname);
            logi.put(Api.key_password, pass);
            logi.put(Api.key_role, "3");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("request", logi.toString());
        return logi;
    }


    private Boolean checkValidation() {
        if (uname == null || uname.isEmpty()) {
            username.requestFocus();
            lEmail.setError("Email can't be empty");
            return false;
        }
//        else if (!Patterns.EMAIL_ADDRESS.matcher(uname).matches()) {
//            lEmail.setError(
//                    String.format(Locale.getDefault(), getString(R.string.error_invalid), "Email"));
//            username.requestFocus();
//            return false;
//        }
        else if (pass == null || pass.isEmpty()) {
            lPass.requestFocus();
            lPass.setError("Password can't be empty");
            return false;
        } else if (pass.length() < 6) {
            lPass.requestFocus();
            lPass.setError("Password must be 6 character long");
            return false;
        }

        return true;
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onClick(View v) {
        if (v == signInBtn) {
            getValues();
            if (checkValidation()) {
                showProgressDialog("Signing In...");

                if (NetworkError.getInstance(this).isOnline()) {
                    login(goLogin());
                } else {
                    ToastBuilder.build(this, " No Internet Connection");
                }

            }
        }

    }
}
