package in.mrpickup.deliveryboy;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import in.mrpickup.deliveryboy.Adapter.ProductEditListAdapter;
import in.mrpickup.deliveryboy.appcontroller.Constant;
import in.mrpickup.deliveryboy.appcontroller.PreferenceManager;
import in.mrpickup.deliveryboy.model.MyOrdersFeed;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MyOrdersDetailActivity extends AppCompatActivity implements View.OnClickListener {

    public TextView orderId, pickAddr, dropAddr, date, name, delType, Category, mStore, mPayment, txtOrdertype,
            txtWeight, txt_kilometer, txt_description, txt_deliveryname;
    public String pickMob, dropLoc,
            pickAddr1, type, delName, poly, approx_dist,
            approx_time, delMob, otp, name1, date11, Category1,
            Storename, mPaymentType, mOrderType, mWeight, mKilometer, mDescription, mDeliveryName, mOrdershow;
    public PreferenceManager mPreference;
    ProductEditListAdapter productEditListAdapter;
    int Oid;
    int status;
    ImageView img_back;
    ArrayList<MyOrdersFeed> mOrderArray = new ArrayList<>();
    ArrayList<MyOrdersFeed> mOrders = new ArrayList<>();
    Button mContinue;
    private Context mContext;
    private boolean pickType, waitTime;
    private LinearLayout mStoreLayouts, mDescriptionLayout;
    private RecyclerView mProductsListview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myorder_details);
        processBundle();
        initObjects();
        initCallbacks();
    }

    private void initObjects() {

        mContext = this;
        orderId = findViewById(R.id.order_id);
        mContinue = findViewById(R.id.continue11);
        img_back = findViewById(R.id.img_back);
        pickAddr = findViewById(R.id.pickupaddress);
        dropAddr = findViewById(R.id.dropaddress);
        date = findViewById(R.id.created_date);
        name = findViewById(R.id.name);
        txtOrdertype = findViewById(R.id.txt_orderType);
        txtWeight = findViewById(R.id.weight);
        delType = findViewById(R.id.type);
        txt_description = findViewById(R.id.txt_descr);
        txt_deliveryname = findViewById(R.id.del_name);
        mStore = findViewById(R.id.store);
        txt_kilometer = findViewById(R.id.txt_Kilometer);
        mPayment = findViewById(R.id.payment_type);
        mStoreLayouts = findViewById(R.id.storelayout);
        mDescriptionLayout = findViewById(R.id.descriptionLayout);
        mProductsListview = findViewById(R.id.products_listview);
        mPreference = new PreferenceManager(mContext);
        mContinue.setVisibility(View.GONE);
        initSetText();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initSetText() {

        orderId.setText(String.valueOf(mOrdershow));
        pickAddr.setText(pickAddr1);
        dropAddr.setText(dropLoc);
        date.setText(date11);
        name.setText(name1);
        String mWeight1 = mWeight;
        String kilometr = mKilometer;
        int index = mWeight1.indexOf(".");
        int index1 = kilometr.indexOf(".");
        String mWeight11 = mWeight1.substring(0, index);
        String kilometr1 = kilometr.substring(0, index);
        txtWeight.setText(mWeight11 + " Kg");
        txt_kilometer.setText(kilometr1 + " km");
        txt_deliveryname.setText(mDeliveryName);

        if (!mDescription.isEmpty()) {
            txt_description.setText(mDescription);
            mDescriptionLayout.setVisibility(View.VISIBLE);
        } else {
            mDescriptionLayout.setVisibility(View.GONE);
        }
        if (pickType) {
            delType.setText("PickUp & Delivery");
            mStoreLayouts.setVisibility(View.GONE);

        } else {
            delType.setText("Buy & Delivery");
            mStoreLayouts.setVisibility(View.VISIBLE);
            mStore.setText(Storename);
        }

        if (mOrderType.equals("OR")) {
            txtOrdertype.setText("Ordinary");
        } else {
            txtOrdertype.setText("Urgent");
        }
        if (mPaymentType.equals("CD")) {
            mPayment.setText("Cash On Delivery");
        } else {
            mPayment.setText("Online");
        }


        mProducts();
    }

    private void mProducts() {
        if (mOrders != null) {
            mProductsListview.setVisibility(View.VISIBLE);
            productEditListAdapter = new ProductEditListAdapter(mContext, mOrders);
            mProductsListview.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, true));
            mProductsListview.setHasFixedSize(true);
            mProductsListview.setScrollBarSize(0);
            mProductsListview.setAdapter(productEditListAdapter);
            productEditListAdapter.notifyDataSetChanged();

        }
    }

    private void initCallbacks() {
        img_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == img_back) {
            onBackPressed();
        }

    }

    private void processBundle() {
        name1 = getIntent().getExtras().getString("NAME");
        pickAddr1 = getIntent().getExtras().getString("PICK_LOC");
        Oid = getIntent().getExtras().getInt("ORDER_ID");
        pickMob = getIntent().getExtras().getString("PICK_MOB");
        delMob = getIntent().getExtras().getString("DELIVERY_MOB");
        dropLoc = getIntent().getExtras().getString("DROP_LOC");
        pickType = getIntent().getExtras().getBoolean("PICK_TYPE");
        status = getIntent().getExtras().getInt("STATUS");
        date11 = getIntent().getExtras().getString("Date");
        delName = getIntent().getExtras().getString("DELIVER_NAME");
        Category1 = getIntent().getExtras().getString("category");
        Storename = getIntent().getExtras().getString("StoreName");
        mPaymentType = getIntent().getExtras().getString("PaymentType");
        mOrderType = getIntent().getExtras().getString("ordertype");
        mWeight = getIntent().getExtras().getString("weight");
        mKilometer = getIntent().getExtras().getString("kilometer");
        mDescription = getIntent().getExtras().getString("Description");
        mDeliveryName = getIntent().getExtras().getString("DELNAME");
        mOrdershow = getIntent().getExtras().getString("ordershow");
        mOrderArray = getIntent().getParcelableArrayListExtra(Constant.TRIP_ARRAY);
        for (int i = 0; i < mOrderArray.size(); i++) {

            if (Oid == mOrderArray.get(i).getmId()) {
                mOrders.add(mOrderArray.get(i));
                Log.e("djfgjdf", "" + mOrders.size());

            }
        }
    }


}
