package in.mrpickup.deliveryboy.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 2/28/2018.
 */

public class EndTrip {


    @SerializedName("is_paid")
    private boolean is_paid;

    public boolean isIs_paid() {
        return is_paid;
    }

    public void setIs_paid(boolean is_paid) {
        this.is_paid = is_paid;
    }
}
