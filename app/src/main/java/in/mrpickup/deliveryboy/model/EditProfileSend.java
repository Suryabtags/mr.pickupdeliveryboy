package in.mrpickup.deliveryboy.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 10/30/2017.
 */

public class EditProfileSend {

    @SerializedName("useraddress")
    private UserAddress userAddress12;


    public EditProfileSend(UserAddress userAddress1) {
        userAddress12 = userAddress1;
    }

    public UserAddress getUserAddress12() {
        return userAddress12;
    }

    public void setUserAddress12(UserAddress userAddress12) {
        this.userAddress12 = userAddress12;
    }


}
