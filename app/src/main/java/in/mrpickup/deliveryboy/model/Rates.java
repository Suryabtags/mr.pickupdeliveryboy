package in.mrpickup.deliveryboy.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 1/3/2018.
 */

public class Rates {


    @SerializedName("id")
    private int mId;
    @SerializedName("from_weight")
    private int mFromWeight;
    @SerializedName("to_weight")
    private int mToWeight;
    @SerializedName("distance_cost")
    private String mDistanceCost;

    @SerializedName("extra_km")
    private String mExtraKm;
    @SerializedName("extra_kg")
    private String mExtraKg;
    @SerializedName("is_active")
    private boolean mIsActive;
    @SerializedName("create_on")
    private String mCreatedOn;


    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public int getmFromWeight() {
        return mFromWeight;
    }

    public void setmFromWeight(int mFromWeight) {
        this.mFromWeight = mFromWeight;
    }

    public int getmToWeight() {
        return mToWeight;
    }

    public void setmToWeight(int mToWeight) {
        this.mToWeight = mToWeight;
    }

    public String getmDistanceCost() {
        return mDistanceCost;
    }

    public void setmDistanceCost(String mDistanceCost) {
        this.mDistanceCost = mDistanceCost;
    }

    public String getmExtraKm() {
        return mExtraKm;
    }

    public void setmExtraKm(String mExtraKm) {
        this.mExtraKm = mExtraKm;
    }

    public String getmExtraKg() {
        return mExtraKg;
    }

    public void setmExtraKg(String mExtraKg) {
        this.mExtraKg = mExtraKg;
    }

    public boolean ismIsActive() {
        return mIsActive;
    }

    public void setmIsActive(boolean mIsActive) {
        this.mIsActive = mIsActive;
    }

    public String getmCreatedOn() {
        return mCreatedOn;
    }

    public void setmCreatedOn(String mCreatedOn) {
        this.mCreatedOn = mCreatedOn;
    }
}
