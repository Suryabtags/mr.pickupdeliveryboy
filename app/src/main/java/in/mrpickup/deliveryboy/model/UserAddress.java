package in.mrpickup.deliveryboy.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 10/30/2017.
 */

public class UserAddress {


    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("street")
    @Expose
    private String street;
    @SerializedName("city")
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("country")
    private String country;

    @SerializedName("area")
    private String zipcode;


    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


}
