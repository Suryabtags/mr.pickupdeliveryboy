package in.mrpickup.deliveryboy.model;

import android.os.Parcel;
import android.os.Parcelable;



public class Feed implements Parcelable {

    public static final Creator<Feed> CREATOR = new Creator<Feed>() {
        @Override
        public Feed createFromParcel(Parcel in) {
            return new Feed(in);
        }

        @Override
        public Feed[] newArray(int size) {
            return new Feed[size];
        }
    };
    private int mId, cId, mStatus;
    private double mStartlat, mStartlng, mEndlat, mEndlng;
    private String mName, mPickLoc, mDropLoc, mPayType, mCategory, mDelType, mDate, mMob, mDelName,
            mStorename, mDMob, mDtype, mSubname, mWeight, mUnits, mDescriptions, mOrderIDShow;
    private boolean mPickType;


    public Feed(int id, String name, String pickMob, String pickLoc, String dropLoc,
                double startlat, double startlng, double endlat, double endlng,
                boolean pickType, int cateId, String category, String delType, String payType, String createdDate,
                int status, String delname, String Sname, String dmob, String dtype, String subname, String weight,
                String units, String descriptions, String Orderid) {
        mId = id;
        mStartlat = startlat;
        mStartlng = startlng;
        mEndlat = endlat;
        mEndlng = endlng;
        mName = name;
        mStatus = status;
        mPickLoc = pickLoc;
        mMob = pickMob;
        mDropLoc = dropLoc;
        mPickType = pickType;
        cId = cateId;
        mCategory = category;
        mDelType = delType;
        mPayType = payType;
        mDate = createdDate;
        mDelName = delname;
        mStorename = Sname;
        mDMob = dmob;
        mDtype = dtype;
        mSubname = subname;
        mWeight = weight;
        mUnits = units;
        mDescriptions = descriptions;
        mOrderIDShow = Orderid;
    }

    protected Feed(Parcel in) {
        mId = in.readInt();
        cId = in.readInt();
        mStatus = in.readInt();
        mStartlat = in.readDouble();
        mStartlng = in.readDouble();
        mEndlat = in.readDouble();
        mEndlng = in.readDouble();
        mName = in.readString();
        mPickLoc = in.readString();
        mDropLoc = in.readString();
        mPayType = in.readString();
        mCategory = in.readString();
        mDelType = in.readString();
        mDate = in.readString();
        mMob = in.readString();
        mDelName = in.readString();
        mStorename = in.readString();
        mDMob = in.readString();
        mDtype = in.readString();
        mSubname = in.readString();
        mWeight = in.readString();
        mUnits = in.readString();
        mDescriptions = in.readString();
        mOrderIDShow = in.readString();
        mPickType = in.readByte() != 0;
    }

    public String getmOrderIDShow() {
        return mOrderIDShow;
    }

    public void setmOrderIDShow(String mOrderIDShow) {
        this.mOrderIDShow = mOrderIDShow;
    }

    public String getmWeight() {
        return mWeight;
    }

    public void setmWeight(String mWeight) {
        this.mWeight = mWeight;
    }

    public String getmDMob() {
        return mDMob;
    }

    public void setmDMob(String mDMob) {
        this.mDMob = mDMob;
    }

    public String getmStorename() {
        return mStorename;
    }

    public void setmStorename(String mStorename) {
        this.mStorename = mStorename;
    }

    public String getmDelName() {
        return mDelName;
    }

    public void setmDelName(String mDelName) {
        this.mDelName = mDelName;
    }

    public int getmStatus() {
        return mStatus;
    }

    public void setmStatus(int mStatus) {
        this.mStatus = mStatus;
    }

    public String getmMob() {
        return mMob;
    }

    public void setmMob(String mMob) {
        this.mMob = mMob;
    }

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public int getcId() {
        return cId;
    }

    public void setcId(int cId) {
        this.cId = cId;
    }

    public double getmStartlat() {
        return mStartlat;
    }

    public void setmStartlat(double mStartlat) {
        this.mStartlat = mStartlat;
    }

    public double getmStartlng() {
        return mStartlng;
    }

    public void setmStartlng(double mStartlng) {
        this.mStartlng = mStartlng;
    }

    public double getmEndlat() {
        return mEndlat;
    }

    public void setmEndlat(double mEndlat) {
        this.mEndlat = mEndlat;
    }

    public double getmEndlng() {
        return mEndlng;
    }

    public void setmEndlng(double mEndlng) {
        this.mEndlng = mEndlng;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmPickLoc() {
        return mPickLoc;
    }

    public void setmPickLoc(String mPickLoc) {
        this.mPickLoc = mPickLoc;
    }

    public String getmDropLoc() {
        return mDropLoc;
    }

    public void setmDropLoc(String mDropLoc) {
        this.mDropLoc = mDropLoc;
    }

    public boolean getmPickType() {
        return mPickType;
    }

    public void setmPickType(boolean mPickType) {
        this.mPickType = mPickType;
    }

    public String getmPayType() {
        return mPayType;
    }

    public void setmPayType(String mPayType) {
        this.mPayType = mPayType;
    }

    public String getmCategory() {
        return mCategory;
    }

    public void setmCategory(String mCategory) {
        this.mCategory = mCategory;
    }

    public String getmDelType() {
        return mDelType;
    }

    public void setmDelType(String mDelType) {
        this.mDelType = mDelType;
    }

    public String getmDate() {
        return mDate;
    }

    public void setmDate(String mDate) {
        this.mDate = mDate;
    }

    public String getmDtype() {
        return mDtype;
    }

    public void setmDtype(String mDtype) {
        this.mDtype = mDtype;
    }


    public String getmSubname() {
        return mSubname;
    }

    public void setmSubname(String mSubname) {
        this.mSubname = mSubname;
    }

    public String getmUnits() {
        return mUnits;
    }

    public void setmUnits(String mUnits) {
        this.mUnits = mUnits;
    }

    public String getmDescriptions() {
        return mDescriptions;
    }

    public void setmDescriptions(String mDescriptions) {
        this.mDescriptions = mDescriptions;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mId);
        dest.writeInt(cId);
        dest.writeInt(mStatus);
        dest.writeDouble(mStartlat);
        dest.writeDouble(mStartlng);
        dest.writeDouble(mEndlat);
        dest.writeDouble(mEndlng);
        dest.writeString(mName);
        dest.writeString(mPickLoc);
        dest.writeString(mDropLoc);
        dest.writeString(mPayType);
        dest.writeString(mCategory);
        dest.writeString(mDelType);
        dest.writeString(mDate);
        dest.writeString(mMob);
        dest.writeString(mDelName);
        dest.writeString(mStorename);
        dest.writeString(mDMob);
        dest.writeString(mDtype);
        dest.writeString(mSubname);
        dest.writeString(mWeight);
        dest.writeString(mUnits);
        dest.writeString(mDescriptions);
        dest.writeString(mOrderIDShow);
        dest.writeByte((byte) (mPickType ? 1 : 0));
    }
}
