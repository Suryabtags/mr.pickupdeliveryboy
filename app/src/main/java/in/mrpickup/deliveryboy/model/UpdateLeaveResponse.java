package in.mrpickup.deliveryboy.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 2/17/2018.
 */

public class UpdateLeaveResponse {


    @SerializedName("userprofile")
    private UserProfiles userprofile;

    public UserProfiles getUserprofile() {
        return userprofile;
    }

    public void setUserprofile(UserProfiles userprofile) {
        this.userprofile = userprofile;
    }

    public class UserProfiles {
        @SerializedName("on_leave")
        private boolean mOnLeave;

        public boolean ismOnLeave() {
            return mOnLeave;
        }

        public void setmOnLeave(boolean mOnLeave) {
            this.mOnLeave = mOnLeave;
        }
    }
}
