package in.mrpickup.deliveryboy.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 10/21/2017.
 */

public class ProfileView {


    @SerializedName("id")
    private int mId;
    @SerializedName("first_name")
    private String first_name;
    @SerializedName("useraddress")
    private UserAddress useraddress;


    @SerializedName("last_name")
    private String last_name;


    @SerializedName("username")
    private String username;

    @SerializedName("email")
    private String email;
    @SerializedName("userprofile")
    private UserProfile userprofile;


    @SerializedName("dob")
    private String dob1;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }


    public UserProfile getUserprofile() {
        return userprofile;
    }

    public void setUserprofile(UserProfile userprofile) {
        this.userprofile = userprofile;
    }

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }


    public UserAddress getUseraddress() {
        return useraddress;
    }

    public void setUseraddress(UserAddress useraddress) {
        this.useraddress = useraddress;
    }


    public class UserAddress {

        @SerializedName("id")
        @Expose
        private String id;

        @SerializedName("street")
        @Expose
        private String street;
        @SerializedName("city")
        private String city;
        @SerializedName("state")
        @Expose
        private String state;
        @SerializedName("country")
        private String country;

        @SerializedName("zipcode")
        private String zipcode;

        @SerializedName("area")
        private String area;
        public String getStreet() {
            return street;
        }

        public void setStreet(String street) {
            this.street = street;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getZipcode() {
            return zipcode;
        }

        public void setZipcode(String zipcode) {
            this.zipcode = zipcode;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getArea() {
            return area;
        }

        public void setArea(String area) {
            this.area = area;
        }
    }


    public class UserProfile {
        @SerializedName("id")
        private String id;


        @SerializedName("profile_pic")
        private String profile_pic;
        @SerializedName("dob")
        private String dob;
        @SerializedName("sex")
        private String sex;

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getProfile_pic() {
            return profile_pic;
        }

        public void setProfile_pic(String profile_pic) {
            this.profile_pic = profile_pic;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }


    }


}
