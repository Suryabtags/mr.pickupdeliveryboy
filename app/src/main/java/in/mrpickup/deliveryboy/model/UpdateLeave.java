package in.mrpickup.deliveryboy.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 2/17/2018.
 */

public class UpdateLeave {


    @SerializedName("on_leave")
    private boolean mOnLeave;

    public UpdateLeave(boolean onleave) {
        mOnLeave = onleave;
    }

}
