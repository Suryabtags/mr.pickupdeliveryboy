package in.mrpickup.deliveryboy.model;

import com.google.gson.annotations.SerializedName;


public class CancelOrder {


    @SerializedName("status")
    private int mStatus;

    @SerializedName("payment_type")
    private String mPaymentType;
    @SerializedName("cancel_reason")
    private String cancel_reason;

    public CancelOrder(int status, String type, String reason) {
        mStatus = status;
        mPaymentType = type;
        cancel_reason = reason;

    }

    public int getmStatus() {
        return mStatus;
    }

    public void setmStatus(int mStatus) {
        this.mStatus = mStatus;
    }

    public String getCancel_reason() {
        return cancel_reason;
    }

    public void setCancel_reason(String cancel_reason) {
        this.cancel_reason = cancel_reason;
    }
}
