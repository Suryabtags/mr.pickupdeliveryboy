package in.mrpickup.deliveryboy.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 1/27/2018.
 */

public class StartTrip {

    @SerializedName("time_taken")
    private String time_taken;

    @SerializedName("polyline")
    private String polyline;


    @SerializedName("approx_dist")
    private String approx_dist;

    public String getTime_taken() {
        return time_taken;
    }

    public void setTime_taken(String time_taken) {
        this.time_taken = time_taken;
    }

    public String getPolyline() {
        return polyline;
    }

    public void setPolyline(String polyline) {
        this.polyline = polyline;
    }

    public String getApprox_dist() {
        return approx_dist;
    }

    public void setApprox_dist(String approx_dist) {
        this.approx_dist = approx_dist;
    }
}
