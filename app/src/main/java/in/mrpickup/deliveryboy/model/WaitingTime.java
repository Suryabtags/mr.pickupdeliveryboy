package in.mrpickup.deliveryboy.model;

import com.google.gson.annotations.SerializedName;



public class WaitingTime {

    @SerializedName("waiting_time")
    private String mWaitingTime;

    @SerializedName("is_first")
    private boolean isFirst;

    public WaitingTime(String waitingtime, boolean isfirst)

    {
        mWaitingTime = waitingtime;
        isFirst = isfirst;
    }

    public boolean isFirst() {
        return isFirst;
    }

    public void setFirst(boolean first) {
        isFirst = first;
    }

    public String getmWaitingTime() {
        return mWaitingTime;
    }

    public void setmWaitingTime(String mWaitingTime) {
        this.mWaitingTime = mWaitingTime;
    }
}
