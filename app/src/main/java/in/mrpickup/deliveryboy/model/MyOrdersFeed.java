package in.mrpickup.deliveryboy.model;

import android.os.Parcel;
import android.os.Parcelable;


public class MyOrdersFeed implements Parcelable {

    public static final Creator<MyOrdersFeed> CREATOR = new Creator<MyOrdersFeed>() {
        @Override
        public MyOrdersFeed createFromParcel(Parcel in) {
            return new MyOrdersFeed(in);
        }

        @Override
        public MyOrdersFeed[] newArray(int size) {
            return new MyOrdersFeed[size];
        }
    };
    private int mId, cId, mStatus;
    private double Mrating;
    private double mStartlat, mStartlng, mEndlat, mEndlng;
    private String mName, mPickLoc, mDropLoc, mPayType, mCategory, mDelType, mDate,
            mMob, mOrderType, mWeight, mSubName,
            mKilometer, mDelname, mStorename, mPaytype, mDelmob, mComments, mQuantity, mDescription, mOrderShow;
    private boolean mPickType;

    public MyOrdersFeed(int id, String name, String pickMob, String pickLoc, String dropLoc,
                        double startlat, double startlng, double endlat, double endlng,
                        boolean pickType, int cateId, String category, String delType, String payType,
                        String createdDate, int status,
                        String dtype, String weight, String mSubname, String kilomter, String delmob, String delname,
                        String Storename, String paytype, double rating, String comments, String quantity,
                        String description, String ordershow) {
        mId = id;
        mStartlat = startlat;
        mStartlng = startlng;
        mEndlat = endlat;
        mEndlng = endlng;
        mName = name;
        mStatus = status;
        mPickLoc = pickLoc;
        mMob = pickMob;
        mDropLoc = dropLoc;
        mPickType = pickType;
        cId = cateId;
        mCategory = category;
        mDelType = delType;
        mPayType = payType;
        mDate = createdDate;
        mOrderType = dtype;
        mWeight = weight;
        mSubName = mSubname;
        mKilometer = kilomter;
        mDelname = delname;
        mDelmob = delmob;
        mStorename = Storename;
        mPaytype = paytype;
        Mrating = rating;
        mComments = comments;
        mQuantity = quantity;
        mDescription = description;
        mOrderShow = ordershow;

    }

    public MyOrdersFeed(Parcel in) {
        mId = in.readInt();
        cId = in.readInt();
        mStatus = in.readInt();
        Mrating = in.readDouble();
        mStartlat = in.readDouble();
        mStartlng = in.readDouble();
        mEndlat = in.readDouble();
        mEndlng = in.readDouble();
        mName = in.readString();
        mPickLoc = in.readString();
        mDropLoc = in.readString();
        mPayType = in.readString();
        mCategory = in.readString();
        mDelType = in.readString();
        mDate = in.readString();
        mMob = in.readString();
        mOrderType = in.readString();
        mWeight = in.readString();
        mSubName = in.readString();
        mKilometer = in.readString();
        mDelname = in.readString();
        mStorename = in.readString();
        mPaytype = in.readString();
        mDelmob = in.readString();
        mComments = in.readString();
        mQuantity = in.readString();
        mDescription = in.readString();
        mOrderShow = in.readString();
        mPickType = in.readByte() != 0;
    }

    public String getmOrderShow() {
        return mOrderShow;
    }

    public void setmOrderShow(String mOrderShow) {
        this.mOrderShow = mOrderShow;
    }

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public String getmDelname() {
        return mDelname;
    }

    public void setmDelname(String mDelname) {
        this.mDelname = mDelname;
    }

    public String getmStorename() {
        return mStorename;
    }

    public void setmStorename(String mStorename) {
        this.mStorename = mStorename;
    }

    public String getmPaytype() {
        return mPaytype;
    }

    public void setmPaytype(String mPaytype) {
        this.mPaytype = mPaytype;
    }

    public String getmDelmob() {
        return mDelmob;
    }

    public void setmDelmob(String mDelmob) {
        this.mDelmob = mDelmob;
    }

    public String getmKilometer() {
        return mKilometer;
    }

    public void setmKilometer(String mKilometer) {
        this.mKilometer = mKilometer;
    }

    public int getmStatus() {
        return mStatus;
    }

    public void setmStatus(int mStatus) {
        this.mStatus = mStatus;
    }

    public String getmMob() {
        return mMob;
    }

    public void setmMob(String mMob) {
        this.mMob = mMob;
    }

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public int getcId() {
        return cId;
    }

    public void setcId(int cId) {
        this.cId = cId;
    }

    public double getmStartlat() {
        return mStartlat;
    }

    public void setmStartlat(double mStartlat) {
        this.mStartlat = mStartlat;
    }

    public double getmStartlng() {
        return mStartlng;
    }

    public void setmStartlng(double mStartlng) {
        this.mStartlng = mStartlng;
    }

    public double getmEndlat() {
        return mEndlat;
    }

    public void setmEndlat(double mEndlat) {
        this.mEndlat = mEndlat;
    }

    public double getmEndlng() {
        return mEndlng;
    }

    public void setmEndlng(double mEndlng) {
        this.mEndlng = mEndlng;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmPickLoc() {
        return mPickLoc;
    }

    public void setmPickLoc(String mPickLoc) {
        this.mPickLoc = mPickLoc;
    }

    public String getmDropLoc() {
        return mDropLoc;
    }

    public void setmDropLoc(String mDropLoc) {
        this.mDropLoc = mDropLoc;
    }

    public boolean getmPickType() {
        return mPickType;
    }

    public void setmPickType(boolean mPickType) {
        this.mPickType = mPickType;
    }

    public String getmPayType() {
        return mPayType;
    }

    public void setmPayType(String mPayType) {
        this.mPayType = mPayType;
    }

    public String getmCategory() {
        return mCategory;
    }

    public void setmCategory(String mCategory) {
        this.mCategory = mCategory;
    }

    public String getmDelType() {
        return mDelType;
    }

    public void setmDelType(String mDelType) {
        this.mDelType = mDelType;
    }

    public String getmDate() {
        return mDate;
    }

    public void setmDate(String mDate) {
        this.mDate = mDate;
    }

    public String getmOrderType() {
        return mOrderType;
    }

    public void setmOrderType(String mOrderType) {
        this.mOrderType = mOrderType;
    }

    public String getmWeight() {
        return mWeight;
    }

    public void setmWeight(String mWeight) {
        this.mWeight = mWeight;
    }


    public String getmSubName() {
        return mSubName;
    }

    public void setmSubName(String mSubName) {
        this.mSubName = mSubName;
    }


    public double getMrating() {
        return Mrating;
    }

    public void setMrating(double mrating) {
        Mrating = mrating;
    }

    public String getmComments() {
        return mComments;
    }

    public void setmComments(String mComments) {
        this.mComments = mComments;
    }

    public String getmQuantity() {
        return mQuantity;
    }

    public void setmQuantity(String mQuantity) {
        this.mQuantity = mQuantity;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mId);
        dest.writeInt(cId);
        dest.writeInt(mStatus);
        dest.writeDouble(Mrating);
        dest.writeDouble(mStartlat);
        dest.writeDouble(mStartlng);
        dest.writeDouble(mEndlat);
        dest.writeDouble(mEndlng);
        dest.writeString(mName);
        dest.writeString(mPickLoc);
        dest.writeString(mDropLoc);
        dest.writeString(mPayType);
        dest.writeString(mCategory);
        dest.writeString(mDelType);
        dest.writeString(mDate);
        dest.writeString(mMob);
        dest.writeString(mOrderType);
        dest.writeString(mWeight);
        dest.writeString(mSubName);
        dest.writeString(mKilometer);
        dest.writeString(mDelname);
        dest.writeString(mStorename);
        dest.writeString(mPaytype);
        dest.writeString(mDelmob);
        dest.writeString(mComments);
        dest.writeString(mQuantity);
        dest.writeString(mDescription);
        dest.writeString(mOrderShow);
        dest.writeByte((byte) (mPickType ? 1 : 0));
    }
}
