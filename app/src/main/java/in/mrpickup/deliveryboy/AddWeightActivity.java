package in.mrpickup.deliveryboy;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.koushikdutta.ion.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import in.mrpickup.deliveryboy.appcontroller.ApiClient;
import in.mrpickup.deliveryboy.appcontroller.ApiInterface;
import in.mrpickup.deliveryboy.appcontroller.ErrorHandler;
import in.mrpickup.deliveryboy.appcontroller.PreferenceManager;
import in.mrpickup.deliveryboy.helper.NetworkError;
import in.mrpickup.deliveryboy.model.StartTrip;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.mrpickup.deliveryboy.helper.Utils.getSeconds;

public class AddWeightActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int REQUEST_IMAGE = 4;
    private static final int REQUEST_WRITE_STORAGE = 4;
    ArrayList<HashMap<String, String>> Category = new ArrayList<>();
    String otp;
    double pickLat, pickLng, dellat, dellng;
    String Picaddress, DeliveryAddress, Dname;
    File file;
    private Button otpBtn;
    private int orderId;
    private boolean pickType;
    private Context mContext;
    private Toolbar mToolbar;
    private ImageView mImageViewMedia;
    private List<File> mImageArrayList;
    private LinearLayout mLayoutMediaContainer;
    private PreferenceManager mPreference;
    private String type, weight, status;
    private ProgressDialog mProgressDialog;
    private Spinner spinnerWeight;
    private TextInputEditText txtWeight;
    private TextInputLayout layoutWeight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_weight);
        processBundle();
        initObj();
        initCallbacks();
        setuptoolbar();

    }

    private void processBundle() {

        pickType = getIntent().getExtras().getBoolean("PICK_TYPE");
        pickLat = getIntent().getExtras().getDouble("PICK_LAT");
        pickLng = getIntent().getExtras().getDouble("PICK_LNG");
        dellat = getIntent().getExtras().getDouble("DEL_LAT");
        dellng = getIntent().getExtras().getDouble("DEL_LNG");
        Picaddress = getIntent().getExtras().getString("PICK_LOC");
        DeliveryAddress = getIntent().getExtras().getString("DROP_LOC");
        Dname = getIntent().getExtras().getString("DELIVER_NAME");
        Log.e("pickLat", "" + pickLat);
        Log.e("pickLng", "" + pickLng);
        Log.e("dellat", "" + dellat);
        Log.e("dellng", "" + dellng);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setuptoolbar() {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setTitle(null);
        mToolbar.setNavigationIcon(R.drawable.ic_back);
    }

    private void initObj() {
        otpBtn = findViewById(R.id.enter_otp);

        mContext = this;
        orderId = getIntent().getExtras().getInt("ORDER_ID");
        mToolbar = findViewById(R.id.toolbar);
        mImageViewMedia = findViewById(R.id.img_media);
        txtWeight = findViewById(R.id.input_weight);
        layoutWeight = findViewById(R.id.layout_weight);
        mImageArrayList = new ArrayList<>();
        mLayoutMediaContainer = findViewById(R.id.media_container);
        mPreference = new PreferenceManager(this);
        mProgressDialog = new ProgressDialog(this);

        if (pickType) {
            otpBtn.setText("ENTER OTP");
        } else {
            otpBtn.setText("SUBMIT");
        }
    }

    private void initCallbacks() {
        otpBtn.setOnClickListener(this);
        mImageViewMedia.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == otpBtn) {
            processInput();
        } else if (v == mImageViewMedia) {
            processPickImages();

        }

    }

    private void processInput() {

        weight = txtWeight.getText().toString().trim();
        if (!pickType) {

            if (mImageArrayList == null || mImageArrayList.isEmpty()) {
                ToastBuilder.build(mContext, "Product Image needed");
            } else {
                if (NetworkError.getInstance(mContext).isOnline()) {
                    startTrip112("");
                } else {
                    ToastBuilder.build(mContext, " No Internet Connection");
                }


            }

        } else if (TextUtils.isEmpty(weight)) {
            txtWeight.requestFocus();
            txtWeight.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty),
                            "Weight"));

        } else if (mImageArrayList == null || mImageArrayList.isEmpty()) {
            ToastBuilder.build(mContext, "Product Image needed");
        } else {
            openOtpDialog();
        }
    }

    public void sendDataBack(String poly, String approx_dist, String time) {
        Intent returnIntent = new Intent(this, MapActivity.class);
        returnIntent.putExtra("STATUS", 7);
        returnIntent.putExtra("POLY_LINE", poly);
        returnIntent.putExtra("APPROX_DIST", approx_dist);
        returnIntent.putExtra("TIME", time);
        returnIntent.putExtra("Otp", otp);
        returnIntent.putExtra("ORDER_ID", orderId);
        returnIntent.putExtra("PICK_TYPE", pickType);
        returnIntent.putExtra("SOURCE_LAT", pickLat);
        returnIntent.putExtra("SOURCE_LNG", pickLng);
        returnIntent.putExtra("DEL_LAT", dellat);
        returnIntent.putExtra("DEL_LNG", dellng);
        returnIntent.putExtra("PICK_LOC", Picaddress);
        returnIntent.putExtra("DROP_LOC", DeliveryAddress);
        returnIntent.putExtra("DELIVER_NAME", Dname);


        startActivity(returnIntent);
        finish();


    }

    private void openOtpDialog() {
        View otpView = LayoutInflater.from(mContext).inflate(R.layout.dialog_otp,
                null);
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setView(otpView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        final EditText txt_Otp = otpView.findViewById(R.id.input_otp);
        final Button startBtn = otpView.findViewById(R.id.start_btn);
        ImageView imageViewClose = otpView.findViewById(R.id.img_close1);
        imageViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otp = txt_Otp.getText().toString().trim();
                if (TextUtils.isEmpty(otp)) {
                    txt_Otp.requestFocus();
                    txt_Otp.setError(
                            String.format(Locale.getDefault(), getString(R.string.error_empty),
                                    "OTP"));
                } else {

                    if (NetworkError.getInstance(mContext).isOnline()) {
                        showProgressDialog("Verifying OTP");
                        startTrip(otp, alertDialog);
                    } else {
                        ToastBuilder.build(mContext, " No Internet Connection");
                    }

                }
            }
        });
    }

    private void startTrip(final String otp, final AlertDialog alertDialog) {

        for (int i = 0; i < mImageArrayList.size(); i++) {
            file = new File(mImageArrayList.get(i).getPath());
        }


        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part imageFileBody = MultipartBody.Part.createFormData("images", file.getName(), requestBody);
        RequestBody Otp = RequestBody.create(MediaType.parse("form-data"), otp);
        RequestBody mWeight = RequestBody.create(MediaType.parse("form-data"), weight);
        RequestBody is_first = RequestBody.create(MediaType.parse("form-data"), "true");
        RequestBody lat = RequestBody.create(MediaType.parse("form-data"), String.valueOf(pickLat));
        RequestBody longq = RequestBody.create(MediaType.parse("form-data"), String.valueOf(pickLng));
        RequestBody order = RequestBody.create(MediaType.parse("form-data"), String.valueOf(orderId));


        ApiInterface newsFeedService = ApiClient.getClient().create(ApiInterface.class);
        Call<StartTrip> call = newsFeedService.startTrip("Token " + mPreference.getToken(),
                Otp, mWeight, is_first, lat, longq, order, imageFileBody);
        call.enqueue(new retrofit2.Callback<StartTrip>() {

            @Override
            public void onResponse(Call<StartTrip> call, retrofit2.Response<StartTrip> response) {
                Log.e("methodresponse", "" + response.message());

                StartTrip starttrip = response.body();
                if (response.isSuccessful() && starttrip != null) {
                    hideProgressDialog();

                    ToastBuilder.build(mContext, "Trip Started");
                    String poly = starttrip.getPolyline();
                    String approx_dist = starttrip.getApprox_dist();
                    String approx = starttrip.getTime_taken();
                    String time = getSeconds(approx);
                    Log.e("poly", "" + poly);
                    Log.e("approx_dist", "" + approx_dist);
                    Log.e("approx", "" + approx);
                    alertDialog.dismiss();
                    mPreference.setOtp(otp);
                    mPreference.setPolyline(poly);
                    sendDataBack(poly, approx_dist, time);

                } else {
                    hideProgressDialog();
                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }

            }

            @Override
            public void onFailure(Call<StartTrip> call, Throwable t) {
                hideProgressDialog();
                Log.e("editprofilefailure", "" + t.getMessage().toString());
            }
        });


    }

    private void handleResponse(Response<String> result) {
        try {
            JSONObject resultObject = new JSONObject(result.getResult());


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void startTrip112(final String otp) {


        for (int i = 0; i < mImageArrayList.size(); i++) {
            file = new File(mImageArrayList.get(i).getPath());
        }
        showProgressDialog("Processing....");

        Log.e("sds", "" + file.getName());
        Log.e("sfdfdsf", "" + pickLng);
        Log.e("sfdfdsf", "" + weight);
        Log.e("sfdfdsf", "" + orderId);
        Log.e("sfdfdsf", "" + pickLat);
        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part imageFileBody = MultipartBody.Part.createFormData("images", file.getName(), requestBody);
        RequestBody Otp = RequestBody.create(MediaType.parse("form-data"), otp);
        RequestBody mWeight = RequestBody.create(MediaType.parse("form-data"), weight);
        RequestBody is_first = RequestBody.create(MediaType.parse("form-data"), "true");

        RequestBody lat = RequestBody.create(MediaType.parse("form-data"), String.valueOf(pickLat));
        RequestBody longq = RequestBody.create(MediaType.parse("form-data"), String.valueOf(pickLng));
        RequestBody order = RequestBody.create(MediaType.parse("form-data"), String.valueOf(orderId));


        ApiInterface newsFeedService = ApiClient.getClient().create(ApiInterface.class);
        Call<StartTrip> call = newsFeedService.startTrip("Token " + mPreference.getToken(),
                Otp, mWeight, is_first, lat, longq, order, imageFileBody);
        call.enqueue(new retrofit2.Callback<StartTrip>() {

            @Override
            public void onResponse(Call<StartTrip> call, retrofit2.Response<StartTrip> response) {
                Log.e("methodresponse", "" + response.message());

                StartTrip starttrip = response.body();
                if (response.isSuccessful() && starttrip != null) {
                    hideProgressDialog();

                    ToastBuilder.build(mContext, "Trip Started");
                    String poly = starttrip.getPolyline();
                    String approx_dist = starttrip.getApprox_dist();
                    String approx = starttrip.getTime_taken();
                    String time = getSeconds(approx);

                    mPreference.setOtp(otp);
                    mPreference.setPolyline(poly);
                    sendDataBack(poly, approx_dist, time);

                } else {
                    hideProgressDialog();
                    ErrorHandler.processError(mContext, response.code(), response.errorBody());

                }
            }

            @Override
            public void onFailure(Call<StartTrip> call, Throwable t) {
                hideProgressDialog();
                Log.e("editprofilefailure", "" + t.getMessage().toString());
            }
        });

    }


    private void checkImage() {
        int imageCount = mImageArrayList.size();
        if (imageCount == 0) {
            mImageViewMedia.setVisibility(View.VISIBLE);
            mLayoutMediaContainer.setVisibility(View.GONE);
        } else {
            mImageViewMedia.setVisibility(View.VISIBLE);
            mLayoutMediaContainer.setVisibility(View.VISIBLE);
        }
    }

    private boolean hasWriteStoragePermission() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED;
    }

    private void requestWriteStoragePermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);
    }

    private void showMedia() {
        mLayoutMediaContainer.removeAllViews();
        checkImage();
        int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100,
                getResources().getDisplayMetrics());
        int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100,
                getResources().getDisplayMetrics());
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(width, height);

        for (int i = 0; i < mImageArrayList.size(); i++) {
            final String path = mImageArrayList.get(i).getPath();
            final View viewImageHolder = LayoutInflater.from(this).inflate(R.layout.item_media,
                    null);
            final ImageView imageViewThumbnail = viewImageHolder.findViewById(
                    R.id.img_media);
            final ImageView imageViewRemove = viewImageHolder.findViewById(
                    R.id.img_remove);

            Glide.with(this).load(path)
                    .apply(new RequestOptions().override(300, 300)).
                    into(imageViewThumbnail);


            imageViewRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int index = ((LinearLayout) viewImageHolder.getParent()).indexOfChild(
                            viewImageHolder);
                    mImageArrayList.remove(index);
                    mLayoutMediaContainer.removeViewAt(index);
                    checkImage();
                }
            });

            imageViewThumbnail.setLayoutParams(layoutParams);
            mLayoutMediaContainer.addView(viewImageHolder);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
                @Override
                public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source,
                                           int type) {
                    mImageArrayList.addAll(imageFiles);
                    showMedia();
                }
            });
        }
    }

    private void processPickImages() {
        if (hasWriteStoragePermission()) {
            pickImages();
        } else {
            requestWriteStoragePermission();
        }
    }

    private void pickImages() {
        EasyImage.configuration(mContext)
                .setImagesFolderName(getString(R.string.app_name))
                .setAllowMultiplePickInGallery(true);
        EasyImage.openChooserWithGallery(this, "Select Images", REQUEST_IMAGE);
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }


}
