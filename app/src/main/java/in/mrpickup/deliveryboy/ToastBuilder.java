package in.mrpickup.deliveryboy;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Admin on 6/12/2017.
 */

public class ToastBuilder {
    public static void build(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
