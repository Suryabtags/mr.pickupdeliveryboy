package in.mrpickup.deliveryboy;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.DynamicDrawableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.widget.TextView;

import com.victor.loading.newton.NewtonCradleLoading;

import in.mrpickup.deliveryboy.appcontroller.MyActivity;
import in.mrpickup.deliveryboy.appcontroller.PreferenceManager;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Splash extends AppCompatActivity {
    TextView mtextviewfooter;
    NewtonCradleLoading newton;
    private PreferenceManager mPreferenceManager;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        mContext = this;
        mPreferenceManager = new PreferenceManager(this);
        checkUserSession();
        mtextviewfooter = findViewById(R.id.billiontags);
        newton = findViewById(R.id.newton_cradle_loading);
        newton.start();
        setFooterTextSpan();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    private void checkUserSession() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mPreferenceManager.getToken() == null) {
                    MyActivity.launch(mContext, Signin.class);

                } else {
                    MyActivity.launch(mContext, Orders.class);

                }
            }
        }, 1000);


    }

    private void setFooterTextSpan() {

        ImageSpan imageSpan = new ImageSpan(mContext, R.drawable.ic_heart,
                DynamicDrawableSpan.ALIGN_BASELINE);
        SpannableString string = new SpannableString("Copy rights by Mr.Pickup");
        string.setSpan(
                new ForegroundColorSpan(ContextCompat.getColor(mContext, R.color.colorPrimary)),
                12, 24, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mtextviewfooter.setText(string);

    }
}

