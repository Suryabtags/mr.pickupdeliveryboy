package in.mrpickup.deliveryboy;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.JsonObject;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import in.mrpickup.deliveryboy.appcontroller.ApiClient;
import in.mrpickup.deliveryboy.appcontroller.ApiInterface;
import in.mrpickup.deliveryboy.appcontroller.PreferenceManager;
import in.mrpickup.deliveryboy.helper.NetworkError;
import in.mrpickup.deliveryboy.model.ProfileView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfile extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {


    private static final int REQUEST_STORAGE_PERMISSION = 4;
    private static final int REQUEST_READ_PERMISSION = 13;
    private static final int REQUEST_FEED_IMAGE = 4;
    ImageView Profile_image_edit_icon, img_back;
    EditText edit_name, edit_Street, edit_City, edit_state, edit_Country, edit_Pincode, edit_area;
    TextView edit_dob;
    TextView usr_phone, usr_mail;
    Button btn_submit_in;
    Context mContext;
    Calendar calendar;
    DatePickerDialog datePickerDialog;
    TextView txt_edit_profilehead;
    File file;
    JsonObject jsonObject;
    private CircleImageView mImageViewProfile;
    private String mImagePath;
    private int mId;
    private String mUrl;
    private ProgressDialog mProgressDialog;
    private int Year, Month, Day;
    private PreferenceManager mPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        initObjects();
        processIntent();

        if (NetworkError.getInstance(this).isOnline()) {
            setUrl();
        } else {
            ToastBuilder.build(this, " No Internet Connection");
        }


    }

    private void initObjects() {
        calendar = Calendar.getInstance();
        Year = calendar.get(Calendar.YEAR);
        Month = calendar.get(Calendar.MONTH);
        Day = calendar.get(Calendar.DAY_OF_MONTH);
        Profile_image_edit_icon = findViewById(R.id.Profile_image_edit_icon);
        img_back = findViewById(R.id.img_back);
        mImageViewProfile = findViewById(R.id.img_profileEdit);
        edit_name = findViewById(R.id.edit_name);
        usr_phone = findViewById(R.id.usr_phone);
        edit_dob = findViewById(R.id.edit_dob);
        usr_mail = findViewById(R.id.usr_mail);
        edit_Street = findViewById(R.id.edit_Street);
        edit_City = findViewById(R.id.edit_City);
        edit_state = findViewById(R.id.edit_state);
        edit_Country = findViewById(R.id.edit_Country);
        edit_Pincode = findViewById(R.id.edit_Pincode);
        edit_area = findViewById(R.id.edit_area);
        btn_submit_in = findViewById(R.id.btn_submit_in);
        mContext = this;
        mPreference = new PreferenceManager(this);
        mProgressDialog = new ProgressDialog(mContext);
        edit_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog = DatePickerDialog.newInstance(EditProfile.this, Year, Month, Day);
                datePickerDialog.setThemeDark(false);
                datePickerDialog.showYearPickerFirst(false);
                datePickerDialog.setAccentColor(Color.parseColor("#EF4438"));
                datePickerDialog.setTitle("Select Date From Calendar");
                datePickerDialog.show(getFragmentManager(), "DatePickerDialog");
            }
        });

        Profile_image_edit_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                processPickImage();
            }
        });
        btn_submit_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                processEditProfile();
            }
        });
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

        int month = monthOfYear + 1;
        Log.e("get", "" + month);
        if (month < 10) {
            edit_dob.setText(year + "-" + ("0" + (monthOfYear + 1)) + "-" + dayOfMonth);

        } else {
            edit_dob.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);

        }

    }

    private void processEditProfile() {
        String firstName = edit_name.getText().toString().trim();
        String dob = edit_dob.getText().toString();
        String mobile = usr_phone.getText().toString();
        String Email = usr_mail.getText().toString();
        if (NetworkError.getInstance(this).isOnline()) {
            geteditprofile(firstName, Email, mobile, dob);
        } else {
            ToastBuilder.build(this, " No Internet Connection");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles,
                                       EasyImage.ImageSource source, int type) {
                mImagePath = imageFiles.get(0).getPath();
                Log.e("imagepathdataprofile", "" + mImagePath);
                file = new File(mImagePath);
                displayImage();

            }
        });
    }

    private void processPickImage() {
        if (hasStoragePermission()) {
            pickImage();
        } else {
            requestStoragePermission(REQUEST_STORAGE_PERMISSION);
        }
    }


    private void pickImage() {
        EasyImage.configuration(mContext).setImagesFolderName(getString(R.string.app_name));
        EasyImage.openChooserWithGallery(this, "Select Image", REQUEST_FEED_IMAGE);
    }

    private boolean hasStoragePermission() {
        return ContextCompat.checkSelfPermission(mContext,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestStoragePermission(int permission) {
        ActivityCompat.requestPermissions(this,
                new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, permission);
    }

    private void setUrl() {
        mUrl = ApiClient.BASE_URL + "user/retrieve/profile/";
        Log.e("mUrl", "" + mUrl);

        getProfilemethod();
    }

    private void displayImage() {
        Glide.with(mContext).load(mImagePath).into(mImageViewProfile);
    }

    private void processIntent() {
        Intent intent = getIntent();
        if (intent != null) {
            String action = intent.getAction();
            if (action != null && action.equals(Intent.ACTION_SEND)) {
                Uri imageUri = intent.getParcelableExtra(Intent.EXTRA_STREAM);
                mImagePath = imageUri.getPath();
                if (hasStoragePermission()) {
                    displayImage();
                } else {
                    requestStoragePermission(REQUEST_READ_PERMISSION);
                }
            }
        }
    }

    private void getProfilemethod() {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ProfileView> call = apiService.getProfile("Token " + mPreference.getToken());
        call.enqueue(new Callback<ProfileView>() {

            @Override
            public void onResponse(Call<ProfileView> call, Response<ProfileView> response) {
                ProfileView profileView = response.body();

                if (response.isSuccessful() && profileView != null) {
                    edit_name.setText(profileView.getFirst_name());
                    edit_dob.setText(profileView.getUserprofile().getDob());
                    usr_phone.setText(profileView.getUsername());
                    usr_mail.setText(profileView.getEmail());
                    edit_Street.setText(profileView.getUseraddress().getStreet());
                    edit_Pincode.setText(profileView.getUseraddress().getZipcode());
                    edit_City.setText(profileView.getUseraddress().getCity());
                    edit_state.setText(profileView.getUseraddress().getState());
                    edit_area.setText(profileView.getUseraddress().getArea());
                    edit_Country.setText(profileView.getUseraddress().getCountry());

                    String imageloader = profileView.getUserprofile().getProfile_pic();
                    if (imageloader != null) {
                        Glide.with(mContext).load(imageloader)
                                .apply(new RequestOptions().placeholder(R.drawable.man).error(R.drawable.man))
                                .into(mImageViewProfile);
                    }

                }
            }

            @Override
            public void onFailure(Call<ProfileView> call, Throwable t) {
                Log.e("failuretokenprofile", "" + t.getMessage());
            }
        });
    }

    public void geteditprofile(final String first_Name, final String email, final String mobile,
                               final String dob) {
        showProgressDialog("Updating");
        JSONObject username = new JSONObject();
        try {
            username.put("street", edit_Street.getText().toString());
            username.put("area", edit_area.getText().toString());
            username.put("city", edit_City.getText().toString());
            username.put("state", edit_state.getText().toString());
            username.put("country", edit_Country.getText().toString());
            username.put("zipcode", edit_Pincode.getText().toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (mImagePath != null) {
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            MultipartBody.Part imageFileBody = MultipartBody.Part.createFormData("profile_pic", file.getName(), requestBody);
            RequestBody first_Name1 = RequestBody.create(MediaType.parse("form-data"), first_Name);
            RequestBody Last_name1 = RequestBody.create(MediaType.parse("form-data"), email);
            RequestBody mobile1 = RequestBody.create(MediaType.parse("form-data"), mobile);
            RequestBody dob1 = RequestBody.create(MediaType.parse("form-data"), dob);
            RequestBody useraddress22 = RequestBody.create(MediaType.parse("multipart/form-data"), username.toString());
            ApiInterface newsFeedService = ApiClient.getClient().create(ApiInterface.class);
            Call<ProfileView> call = newsFeedService.profileedit("Token " + mPreference.getToken(),
                    first_Name1, Last_name1, mobile1, useraddress22, imageFileBody, dob1);
            call.enqueue(new retrofit2.Callback<ProfileView>() {

                @Override
                public void onResponse(Call<ProfileView> call, Response<ProfileView> response) {
                    Log.e("methodresponse", "" + response.message());

                    ProfileView profileView = response.body();
                    if (response.isSuccessful() && profileView != null) {
                        hideProgressDialog();

                        edit_name.setText(profileView.getFirst_name());
                        edit_dob.setText(profileView.getUserprofile().getDob());
                        usr_phone.setText(profileView.getUsername());
                        usr_mail.setText(profileView.getEmail());
                        edit_Street.setText(profileView.getUseraddress().getStreet());
                        edit_Pincode.setText(profileView.getUseraddress().getZipcode());
                        edit_City.setText(profileView.getUseraddress().getCity());
                        edit_state.setText(profileView.getUseraddress().getState());
                        edit_area.setText(profileView.getUseraddress().getArea());
                        edit_Country.setText(profileView.getUseraddress().getCountry());
                        onBackPressed();

                    } else {
                        hideProgressDialog();
                        try {
                            Log.e("editpro", "" + response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }

                @Override
                public void onFailure(Call<ProfileView> call, Throwable t) {
                    hideProgressDialog();
                    Log.e("editprofilefailure", "" + t.getMessage().toString());
                }
            });
        } else {

            RequestBody first_Name1 = RequestBody.create(MediaType.parse("form-data"), first_Name);
            RequestBody Last_name1 = RequestBody.create(MediaType.parse("form-data"), email);
            RequestBody mobile2 = RequestBody.create(MediaType.parse("form-data"), mobile);
            RequestBody dob1 = RequestBody.create(MediaType.parse("form-data"), dob);
            RequestBody useraddress22 = RequestBody.create(MediaType.parse("multipart/form-data"), username.toString());
            ApiInterface newsFeedService = ApiClient.getClient().create(ApiInterface.class);

            Call<ProfileView> call = newsFeedService.profileedit1("Token " + mPreference.getToken(),
                    first_Name1, Last_name1, mobile2, useraddress22, dob1);
            call.enqueue(new retrofit2.Callback<ProfileView>() {

                @Override
                public void onResponse(Call<ProfileView> call, Response<ProfileView> response) {
                    ProfileView profileView = response.body();
                    if (response.isSuccessful() && profileView != null) {
                        hideProgressDialog();
                        Toast.makeText(mContext, " UpdateSucessfully", Toast.LENGTH_SHORT).show();
                        edit_name.setText(profileView.getFirst_name());
                        edit_dob.setText(profileView.getUserprofile().getDob());
                        usr_phone.setText(profileView.getUsername());
                        usr_mail.setText(profileView.getEmail());
                        edit_Street.setText(profileView.getUseraddress().getStreet());
                        edit_Pincode.setText(profileView.getUseraddress().getZipcode());
                        edit_City.setText(profileView.getUseraddress().getCity());
                        edit_state.setText(profileView.getUseraddress().getState());
                        edit_area.setText(profileView.getUseraddress().getArea());
                        edit_Country.setText(profileView.getUseraddress().getCountry());


                        onBackPressed();
                    } else {
                        hideProgressDialog();
                        try {
                            Log.e("elseeditprofileerror111", "" + response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }

                @Override
                public void onFailure(Call<ProfileView> call, Throwable t) {
                    hideProgressDialog();
                    Log.e("editprofilefailure111", "" + t.getMessage().toString());
                }
            });
        }

    }


    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
