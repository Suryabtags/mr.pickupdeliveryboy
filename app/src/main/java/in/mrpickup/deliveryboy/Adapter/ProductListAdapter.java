package in.mrpickup.deliveryboy.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import in.mrpickup.deliveryboy.Holder.AddProductHolder;
import in.mrpickup.deliveryboy.R;
import in.mrpickup.deliveryboy.model.Feed;


public class ProductListAdapter extends RecyclerView.Adapter<AddProductHolder> {

    String catname, subcatname, mQuantity, id, productId;
    private Context mContext;
    private List<Feed> mProductList;

    public ProductListAdapter(Context context, List<Feed> chatList) {
        mContext = context;
        mProductList = chatList;
    }

    @Override
    public AddProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AddProductHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_editproducts, parent,
                        false));
    }

    @Override
    public void onBindViewHolder(final AddProductHolder holder, @SuppressLint("RecyclerView") final int position) {
        Feed productdata = mProductList.get(position);

        holder.mCategory.setText(productdata.getmCategory());
        holder.mSubCategory.setText(productdata.getmSubname());
        holder.mQuantity.setText(productdata.getmUnits());

    }


    @Override
    public int getItemCount() {
        return mProductList.size();
    }
}
