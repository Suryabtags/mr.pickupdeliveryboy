package in.mrpickup.deliveryboy.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;

import in.mrpickup.deliveryboy.Holder.OrderfeedHolder;
import in.mrpickup.deliveryboy.R;
import in.mrpickup.deliveryboy.appcontroller.ApiClient;
import in.mrpickup.deliveryboy.appcontroller.ApiInterface;
import in.mrpickup.deliveryboy.appcontroller.PreferenceManager;
import in.mrpickup.deliveryboy.callback.OrderfeedCallback;
import in.mrpickup.deliveryboy.model.CancelOrder;
import in.mrpickup.deliveryboy.model.Feed;
import retrofit2.Call;

/**
 * Created by sase on 09-06-2017
 */

public class OrderFeedAdapter extends RecyclerView.Adapter<OrderfeedHolder> {

    Dialog dialog;
    Feed feed;
    private List<Feed> mFeedList;
    private OrderfeedCallback mCallback;
    private Context mContext;
    private PreferenceManager mPreference;
    private EditText mDialogReason;

    public OrderFeedAdapter(Context context, List<Feed> feedList, OrderfeedCallback callback) {
        mFeedList = feedList;
        mCallback = callback;
        mContext = context;

        dialog = new Dialog(mContext);
    }

    @Override
    public OrderfeedHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new OrderfeedHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_recycle_content1, parent, false),
                mCallback);
    }


    @Override
    public void onBindViewHolder(OrderfeedHolder holder, final int position) {
        feed = mFeedList.get(position);
        holder.orderId.setText("Order Id: " + feed.getmOrderIDShow());
        holder.pickAddr.setText(feed.getmPickLoc());
        holder.dropAddr.setText(feed.getmDropLoc());
        holder.date.setText(feed.getmDate());
        holder.name.setText(feed.getmName());
//        holder.category.setText(feed.getmCategory());
//        holder.payType.setText(feed.getmPayType());
//        holder.delType.setText(feed.getmDelType());
        mPreference = new PreferenceManager(mContext);
        if (feed.getmPickType()) {
            holder.mTripType.setText("Pick up & Delivery " + "(Order Type: " + feed.getmDtype() + " )");
            holder.mTripType.setTextColor(ContextCompat.getColor(mContext, R.color.accentLight));
        } else {
            holder.mTripType.setText("Buy & Delivery " + "(Order Type: " + feed.getmDtype() + " )");
            holder.mTripType.setTextColor(ContextCompat.getColor(mContext, R.color.accent_green));
        }
        if (feed.getmStatus() == 7) {
            holder.mTripStatus.setVisibility(View.VISIBLE);
            holder.mTripOrderInfo.setVisibility(View.VISIBLE);
        } else {
            holder.mTripStatus.setVisibility(View.GONE);
            holder.mTripOrderInfo.setVisibility(View.GONE);
        }

        holder.cancel_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.setCancelable(true);
                dialog.setContentView(R.layout.cancel_order_dialog);
                dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

                mDialogReason = dialog.findViewById(R.id.dialog_reason);
                TextView mbtn_Yes = dialog.findViewById(R.id.Apply_btn);
                TextView mbtn_No = dialog.findViewById(R.id.Cancel_btn);

                mbtn_Yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String reason = mDialogReason.getText().toString().trim();
                        Log.e("reason", "" + reason);
                        if (reason.isEmpty()) {
                            mDialogReason.setError("Please Enter the Reason to cancel the order");

                        } else {
                            CancelOrsers(mFeedList.get(position).getmId(), new CancelOrder(8, "", reason));
                            removeItem(position);
                            dialog.dismiss();
                        }

                    }
                });

                mbtn_No.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        dialog.dismiss();
                    }
                });
                dialog.show();


            }
        });

    }


    private void removeItem(int position) {
        mFeedList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mFeedList.size());
    }

    @Override
    public int getItemCount() {
        return mFeedList.size();
    }


    public void CancelOrsers(int id, CancelOrder cancelOrder) {

        String mUrl = ApiClient.BASE_URL + "orders/" + id + "/change_status/";
        ApiInterface authService = ApiClient.getClient().create(ApiInterface.class);
        Call<Void> call = authService.getCancelOrder(mUrl, "Token " + mPreference.getToken(), cancelOrder);

        call.enqueue(new retrofit2.Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, retrofit2.Response<Void> response) {
                if (response.isSuccessful()) {
                    Log.e("sucessdata", "" + response.toString());

                } else {
                    try {
                        Log.e("sucessdata", "" + response.toString());
                        Log.e("delrdsdf", "" + response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }

}
