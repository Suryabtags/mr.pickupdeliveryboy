package in.mrpickup.deliveryboy.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import in.mrpickup.deliveryboy.Holder.AddProductHolder;
import in.mrpickup.deliveryboy.R;
import in.mrpickup.deliveryboy.model.MyOrdersFeed;


public class ProductEditListAdapter extends RecyclerView.Adapter<AddProductHolder> {

    String catname, subcatname, mQuantity, id, productId;
    private Context mContext;
    private List<MyOrdersFeed> mProductList;

    public ProductEditListAdapter(Context context, List<MyOrdersFeed> chatList) {
        mContext = context;
        mProductList = chatList;
    }

    @Override
    public AddProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AddProductHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_editproducts, parent,
                        false));
    }

    @Override
    public void onBindViewHolder(final AddProductHolder holder, @SuppressLint("RecyclerView") final int position) {
        MyOrdersFeed productdata = mProductList.get(position);

        holder.mCategory.setText(productdata.getmCategory());
        holder.mSubCategory.setText(productdata.getmSubName());
        holder.mQuantity.setText(productdata.getmQuantity());

    }


    @Override
    public int getItemCount() {
        return mProductList.size();
    }
}
