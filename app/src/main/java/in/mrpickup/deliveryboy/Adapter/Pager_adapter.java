package in.mrpickup.deliveryboy.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

/**
 * Created by sase on 07-06-2017
 */

public class Pager_adapter extends FragmentStatePagerAdapter {
    private List<Fragment> fraglist;

    public Pager_adapter(FragmentManager fm, List<Fragment> fraglist2) {
        super(fm);
        fraglist = fraglist2;
    }

    @Override
    public Fragment getItem(int position) {
        return fraglist.get(position);
    }

    public int getItemPosition(Object ob) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return fraglist.size();

    }

}
