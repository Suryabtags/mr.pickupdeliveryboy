package in.mrpickup.deliveryboy.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import in.mrpickup.deliveryboy.Holder.MyOrderfeedHolder;
import in.mrpickup.deliveryboy.R;
import in.mrpickup.deliveryboy.callback.OrderfeedCallback;
import in.mrpickup.deliveryboy.model.MyOrdersFeed;


public class MyOrderAdapter extends RecyclerView.Adapter<MyOrderfeedHolder> {

    Dialog dialog;

    private List<MyOrdersFeed> mFeedList;
    private OrderfeedCallback mCallback;
    private Context mContext;
    private EditText mDialogReason;

    public MyOrderAdapter(Context context, List<MyOrdersFeed> feedList, OrderfeedCallback callback) {
        mFeedList = feedList;
        mCallback = callback;
        mContext = context;

    }

    @Override
    public MyOrderfeedHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyOrderfeedHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_recycle_content2, parent, false),
                mCallback);
    }

    @Override
    public void onBindViewHolder(MyOrderfeedHolder holder, final int position) {
        final MyOrdersFeed feed = mFeedList.get(position);
        holder.orderId.setText("Order Id: " + feed.getmOrderShow());
        holder.pickAddr.setText(feed.getmPickLoc());
        holder.dropAddr.setText(feed.getmDropLoc());
        holder.date.setText(feed.getmDate());
        holder.name.setText(feed.getmName());
        if (feed.getMrating() > 0) {
            double rating = feed.getMrating();
            holder.mRatingBar.setRating((float) rating);
            holder.mRatingBar.setVisibility(View.VISIBLE);
        } else {
            holder.mRatingBar.setVisibility(View.GONE);
        }
        Log.e("feed", "" + feed.getmComments());
        if (!feed.getmComments().isEmpty() && !feed.getmComments().equals("null") && feed.getmComments() != null) {
            holder.ViewComments.setVisibility(View.VISIBLE);
            holder.ViewComments.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    opencommentsDialog(feed.getmComments());
                }
            });

        } else {
            holder.ViewComments.setVisibility(View.GONE);
        }


        if (feed.getmStatus() == 4) {
            holder.mPayment.setText("Cash ");

        } else if (feed.getmStatus() == 11) {
            holder.mPayment.setText("Paid");
        } else {
            holder.mPayment.setText("Online ");
        }
//        holder.category.setText(feed.getmCategory());
//        holder.payType.setText(feed.getmPayType());
//        holder.delType.setText(feed.getmDelType());

    }


    @Override
    public int getItemCount() {
        return mFeedList.size();
    }

    private void opencommentsDialog(String comments) {
        View otpView = LayoutInflater.from(mContext).inflate(R.layout.dialog_comments,
                null);
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setView(otpView);
        final AlertDialog alertDialog = builder.create();

        Log.e("valuies", "" + comments);
        alertDialog.show();
        TextView txt_Otp = otpView.findViewById(R.id.comments);
        txt_Otp.setText(comments);
        ImageView imageViewClose = otpView.findViewById(R.id.img_close1);
        imageViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

    }

}
