package in.mrpickup.deliveryboy;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.async.http.body.Part;
import com.koushikdutta.async.http.body.StringPart;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import in.mrpickup.deliveryboy.appcontroller.ApiClient;
import in.mrpickup.deliveryboy.appcontroller.ApiInterface;
import in.mrpickup.deliveryboy.appcontroller.ErrorHandler;
import in.mrpickup.deliveryboy.appcontroller.MyActivity;
import in.mrpickup.deliveryboy.appcontroller.PreferenceManager;
import in.mrpickup.deliveryboy.helper.NetworkError;
import in.mrpickup.deliveryboy.model.CancelOrder;
import in.mrpickup.deliveryboy.model.EndTrip;
import in.mrpickup.deliveryboy.model.WaitingTime;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.mrpickup.deliveryboy.VolleyErrorHandler.processError;
import static in.mrpickup.deliveryboy.appcontroller.Api.KEY_APPROX;
import static in.mrpickup.deliveryboy.appcontroller.Api.KEY_ISFIRST;
import static in.mrpickup.deliveryboy.appcontroller.Api.KEY_LATITUDE;
import static in.mrpickup.deliveryboy.appcontroller.Api.KEY_LONGITUDE;
import static in.mrpickup.deliveryboy.appcontroller.Api.KEY_ORDERIDNEW;
import static in.mrpickup.deliveryboy.appcontroller.Api.KEY_OTP;
import static in.mrpickup.deliveryboy.appcontroller.Api.KEY_POLYLINE;
import static in.mrpickup.deliveryboy.appcontroller.Api.KEY_TIMETAKEN;
import static in.mrpickup.deliveryboy.appcontroller.Api.OTP_VERIFY;
import static in.mrpickup.deliveryboy.helper.Utils.getSeconds;

public class MapActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener, OnMapReadyCallback, View.OnClickListener {
    private static final int REQUEST_PERMISSION_LOCATION = 4;
    private static final int REQUEST_ENABLE_LOCATION = 4;

    private static final int REQUEST_IMAGE = 4;
    private static final int REQUEST_WRITE_STORAGE = 4;
    private static final String TAG = null;
    public String endTrip = "End Trip";
    public String pickMob, dropLoc, pickAddr, type, delName, poly, approx_dist, approx_time, delMob, otp, ordershow;
    String name;
    Dialog alertDialog;
    String mStartWaitTime;
    Timer myTimer;
    ImageView mImageViewMedia;
    AlertDialog alertDialogImage;
    File file;
    Chronometer imageclock;
    int time = 60;
    String PaymentMode = "", PaymentType, mstatus;
    AlertDialog alertDialog2;
    long backpresstiming;
    long totalseconds;
    long stopTime = 0;
    private Marker mCurrLocationMarker;
    private Location mUserLocation;
    private double pickLat, pickLng, droplat, droplng;
    private LinearLayout mLayout;
    private TextView txtBtn, txt_name, txtAddr, txtDist, txtTime, start, weight, txtOrderId;
    private ImageView imgCall, path;
    private SupportMapFragment mapFragment;
    private int Oid, status;
    private boolean pickType, waitTime;
    private ProgressBar mProgressBar;
    private Context mContext;
    private PreferenceManager mPreference;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private double mLat1, mLng1;
    private double mLat2, mLng2;
    private FloatingActionButton locBtn;
    private GoogleMap mMap;
    private ProgressDialog mProgressDialog;
    private String Waitingtime, WaitingTime1;
    private boolean currentWaiting;
    private List<File> mImageArrayList;
    private LinearLayout mLayoutMediaContainer;
    private String WaitingTimenew = "";
    private CountDownTimer countDownTimer;
    private boolean timerStart = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        initObjects();
        processBundle();
        initCall();
        initLocationRequest();
        buildGoogleApiClient();
        displayLocationSettingsRequest();

    }

    private void processBundle() {
        name = getIntent().getExtras().getString("NAME");
        pickAddr = getIntent().getExtras().getString("PICK_LOC");
        Oid = getIntent().getExtras().getInt("ORDER_ID");
        pickMob = getIntent().getExtras().getString("PICK_MOB");
        delMob = getIntent().getExtras().getString("DELIVERY_MOB");
        dropLoc = getIntent().getExtras().getString("DROP_LOC");
        pickType = getIntent().getExtras().getBoolean("PICK_TYPE");
        pickLat = getIntent().getExtras().getDouble("SOURCE_LAT");
        pickLng = getIntent().getExtras().getDouble("SOURCE_LNG");
        status = getIntent().getExtras().getInt("STATUS");
        droplat = getIntent().getExtras().getDouble("DEL_LAT");
        droplng = getIntent().getExtras().getDouble("DEL_LNG");
        delName = getIntent().getExtras().getString("DELIVER_NAME");
        approx_dist = getIntent().getExtras().getString("APPROX_DIST");
        approx_time = getIntent().getExtras().getString("TIME");
        ordershow = getIntent().getExtras().getString("ordershow");
        poly = getIntent().getExtras().getString("POLY_LINE");

        otp = mPreference.getotp();
        mPreference.setStartWait(String.valueOf(Calendar.getInstance().getTimeInMillis()));
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initObjects() {

        mContext = this;
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mLocationRequest = new LocationRequest();
        mUserLocation = new Location("user");
        locBtn = findViewById(R.id.locBtn);
        txt_name = findViewById(R.id.txt_name);
        txtAddr = findViewById(R.id.txt_address);
        txtDist = findViewById(R.id.txt_dist);
        txtTime = findViewById(R.id.txt_time);
        start = findViewById(R.id.startwtng);
        imageclock = findViewById(R.id.imageclock);
        weight = findViewById(R.id.addweight);
        txtOrderId = findViewById(R.id.txt_orderId);
        imgCall = findViewById(R.id.callpick);
        mLayout = findViewById(R.id.layout_api);
        mLayout.setVisibility(View.INVISIBLE);
        mProgressBar = findViewById(R.id.progress);
        mProgressBar.setVisibility(View.VISIBLE);
        mPreference = new PreferenceManager(this);
        mProgressDialog = new ProgressDialog(this);
        path = findViewById(R.id.route);

        myTimer = new Timer();
    }

    private void initCall() {

        locBtn.setOnClickListener(this);
        imgCall.setOnClickListener(this);
        start.setOnClickListener(this);
        weight.setOnClickListener(this);
        weight.setVisibility(View.VISIBLE);
        path.setOnClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null && !mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();

        }
        //  processWaittime();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            startLocationUpdates();

        }

    }

    private void setMarker(Double mlat, Double mlng) {
        LatLng latLng = new LatLng(mlat, mlng);
        if (mCurrLocationMarker == null) {
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
            mCurrLocationMarker = mMap.addMarker(markerOptions);
            //move map camera
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 11));

        } else mCurrLocationMarker.setPosition(latLng);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));

    }

    private void movecameradefault(double latitude, double longitude) {
        moveCamera(latitude, longitude);
    }

    private void moveCamera(Double lat, Double lng) {
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 17));
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();

        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
        } else {
            getCurrentLocation();
        }
        if (mUserLocation != null)
            if (status == 7 && !pickType) {
                processWaittime();
                startTrip("", Oid);
                mPreference.setOrderId(Oid);
                weight.setText(endTrip);
                //   mPreference.setOrderWait(0);
                // start.setVisibility(View.VISIBLE);
                txtAddr.setText(dropLoc);
                txt_name.setText(name);
                txtOrderId.setText("Order Id:" + ordershow);

            } else if (status == 7) {
                processWaittime();
                mPreference.setOrderId(Oid);

                startTrip(mPreference.getotp(), Oid);
                weight.setText(endTrip);
                //  mPreference.setOrderWait(0);
                //    start.setVisibility(View.VISIBLE);
                txtAddr.setText(dropLoc);
                txt_name.setText(delName);
                txtOrderId.setText("Order Id:" + ordershow);

            } else if (status == 2) {
                mPreference.setOrderId(Oid);
                mPreference.setOrderWait(0);
                mProgressBar.setVisibility(View.GONE);
                mLayout.setVisibility(View.VISIBLE);
                weight.setText("Add Weight");
                start.setVisibility(View.GONE);
                txtAddr.setText(pickAddr);
                txt_name.setText(name);
                txtOrderId.setText("Order Id:" + ordershow);
            }

    }

    private void getCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mUserLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mUserLocation != null) {
            //Getting longitude and latitude
            double longitude = mUserLocation.getLongitude();
            double latitude = mUserLocation.getLatitude();
            setMarker(latitude, longitude);//moving the map to mUserLocation
        }
        movecameradefault(mUserLocation.getLatitude(), mUserLocation.getLongitude());
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        ToastBuilder.build(mContext, connectionResult.getErrorMessage());
    }

    @Override
    public void onLocationChanged(Location location) {
        this.mUserLocation = location;
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        if (mCurrLocationMarker == null) {
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.title("Current Position");
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
            mCurrLocationMarker = mMap.addMarker(markerOptions);
        } else mCurrLocationMarker.setPosition(latLng);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 17));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ENABLE_LOCATION:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    displayLocationSettingsRequest();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void initLocationRequest() {
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    private void displayLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder =
                new LocationSettingsRequest.Builder()
                        .addLocationRequest(mLocationRequest)
                        .setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient,
                        builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(MapActivity.this,
                                    REQUEST_ENABLE_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        });
    }

    private void startLocationUpdates() {
        if (ContextCompat.checkSelfPermission(mContext,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                    mLocationRequest, this);
        } else {
            requestLocationPermission();
        }
    }

    private void requestLocationPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                REQUEST_PERMISSION_LOCATION);
    }

    private void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        try {

            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.mapstyle));

            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {

                mMap.setMyLocationEnabled(true);
            }
        } else {

            mMap.setMyLocationEnabled(true);
        }

    }

    private void startTrip(String otp, int mOrderId) {

        ArrayList<Part> partArrayList = new ArrayList<>();
        if (otp != null)
            partArrayList.add(new StringPart(KEY_OTP, otp));
        partArrayList.add(new StringPart(KEY_ISFIRST, "false"));
        partArrayList.add(new StringPart(KEY_LATITUDE, String.valueOf(mUserLocation.getLatitude())));
        partArrayList.add(new StringPart(KEY_LONGITUDE, String.valueOf(mUserLocation.getLongitude())));
        partArrayList.add(new StringPart(KEY_ORDERIDNEW, String.valueOf(mOrderId)));
        Ion.with(mContext)
                .load("POST", OTP_VERIFY)
                .addHeader("Authorization", "Token " + mPreference.getToken())
                .addMultipartParts(partArrayList)
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<com.koushikdutta.ion.Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, com.koushikdutta.ion.Response<String> result) {
                        hideProgressDialog();
                        if (e == null) {
                            int code = result.getHeaders().code();
                            if (code == 200 || code == 201) {
                                mProgressBar.setVisibility(View.GONE);
                                mLayout.setVisibility(View.VISIBLE);
                                handleResponse(result);
                            } else {
                                Log.e("fgfg", "" + result.getResult());
                                processError(mContext, code, result.getResult());
                            }
                        } else {

                            ToastBuilder.build(mContext, e.getMessage());
                        }
                    }
                });
    }

    private void handleResponse(com.koushikdutta.ion.Response<String> result) {
        try {
            JSONObject resultObject = new JSONObject(result.getResult());
            String polyLine = resultObject.getString(KEY_POLYLINE);
            String approxDist = resultObject.getString(KEY_APPROX);
            String timeTaken = getSeconds(resultObject.getString(KEY_TIMETAKEN));
            txtDist.setText(String.format(Locale.getDefault(), mContext.getString(R.string.format_approx_dist), approxDist));
            txtTime.setText(String.format(Locale.getDefault(), mContext.getString(R.string.format_approx_time), timeTaken));

            drawOnMapOne(polyLine);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void drawOnMapOne(String poly) {
        List<LatLng> decodedPath = PolyUtil.decode(poly);

        mMap.addPolyline(new PolylineOptions().color(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary)).addAll(decodedPath));
    }

    private void openOtpDialog() {
        View otpView = LayoutInflater.from(mContext).inflate(R.layout.dialog_end_otp, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setView(otpView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        final EditText txt_Otp = otpView.findViewById(R.id.input_otp);
        final Button startBtn = otpView.findViewById(R.id.start_btn);
        ImageView imageViewClose = otpView.findViewById(R.id.img_close1);


        if (mImageArrayList.size() > 0) {
            alertDialog.setCancelable(false);
        } else {
            alertDialog.setCancelable(true);
        }
        imageViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String otp = txt_Otp.getText().toString().trim();
                if (TextUtils.isEmpty(otp)) {
                    txt_Otp.requestFocus();
                    txt_Otp.setError(
                            String.format(Locale.getDefault(), getString(R.string.error_empty),
                                    "OTP"));
                } else {
                    myTimer.cancel();
                    myTimer.purge();
                    getWaitingTime();

                    imageclock.stop();
                    if (NetworkError.getInstance(mContext).isOnline()) {
                        verifyOtpnew(otp, alertDialog);
                    } else {
                        ToastBuilder.build(mContext, " No Internet Connection");
                    }

                    showProgressDialog("Verifying OTP");

                }
            }
        });


    }

    private void openEndTripImageDialog() {
        View otpView = LayoutInflater.from(mContext).inflate(R.layout.dialog_endtrip_image, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setView(otpView);
        alertDialogImage = builder.create();
        alertDialogImage.show();
        alertDialogImage.setCancelable(false);
        mImageViewMedia = otpView.findViewById(R.id.img_media);
        mLayoutMediaContainer = otpView.findViewById(R.id.media_container);
        mImageArrayList = new ArrayList<>();
        final Button startBtn = otpView.findViewById(R.id.start_btn);
        ImageView imageViewClose = otpView.findViewById(R.id.img_close1);
        imageViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogImage.dismiss();
            }
        });

        mImageViewMedia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                processPickImages();
            }
        });
        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mImageArrayList == null || mImageArrayList.isEmpty()) {
                    ToastBuilder.build(mContext, "Product Image needed");
                } else {
                    alertDialogImage.dismiss();
                    openOtpDialog();
                }
            }
        });


    }

    private void processPickImages() {
        if (hasWriteStoragePermission()) {
            pickImages();
        } else {
            requestWriteStoragePermission();
        }
    }

    private void pickImages() {
        EasyImage.configuration(mContext)
                .setImagesFolderName(getString(R.string.app_name))
                .setAllowMultiplePickInGallery(true);
        EasyImage.openChooserWithGallery(this, "Select Images", REQUEST_IMAGE);
    }

    private boolean hasWriteStoragePermission() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED;
    }

    private void requestWriteStoragePermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
                @Override
                public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source,
                                           int type) {
                    mImageArrayList.addAll(imageFiles);
                    Log.e("mImageArrayList", "" + mImageArrayList.size());
                    showMedia();
                }
            });
        }
    }

    private void showMedia() {
        mLayoutMediaContainer.removeAllViews();
        checkImage();
        int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100,
                getResources().getDisplayMetrics());
        int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100,
                getResources().getDisplayMetrics());
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(width, height);

        for (int i = 0; i < mImageArrayList.size(); i++) {
            final String path = mImageArrayList.get(i).getPath();

            final View viewImageHolder = LayoutInflater.from(this).inflate(R.layout.item_media,
                    null);
            final ImageView imageViewThumbnail = viewImageHolder.findViewById(
                    R.id.img_media);
            final ImageView imageViewRemove = viewImageHolder.findViewById(
                    R.id.img_remove);

            Glide.with(this).load(path).apply(new RequestOptions().override(300, 300))
                    .into(imageViewThumbnail);


            imageViewRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int index = ((LinearLayout) viewImageHolder.getParent()).indexOfChild(
                            viewImageHolder);
                    mImageArrayList.remove(index);
                    mLayoutMediaContainer.removeViewAt(index);
                    checkImage();
                }
            });

            imageViewThumbnail.setLayoutParams(layoutParams);
            mLayoutMediaContainer.addView(viewImageHolder);
        }
    }

    private void checkImage() {
        int imageCount = mImageArrayList.size();
        if (imageCount == 0) {
            mImageViewMedia.setVisibility(View.VISIBLE);
            mLayoutMediaContainer.setVisibility(View.GONE);
        } else {
            mImageViewMedia.setVisibility(View.VISIBLE);
            mLayoutMediaContainer.setVisibility(View.VISIBLE);
        }
    }

    private void verifyOtpnew(String otp, final AlertDialog alertDialog) {
        double myLng = mUserLocation.getLongitude();
        double myLat = mUserLocation.getLatitude();
        for (int i = 0; i < mImageArrayList.size(); i++) {
            file = new File(mImageArrayList.get(i).getPath());
        }
        RequestBody Waitingtime1;
        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part imageFileBody = MultipartBody.Part.createFormData("images", file.getName(), requestBody);
        RequestBody Otp = RequestBody.create(MediaType.parse("form-data"), otp);
        RequestBody is_first = RequestBody.create(MediaType.parse("form-data"), "true");
        RequestBody lat = RequestBody.create(MediaType.parse("form-data"), String.valueOf(myLat));
        RequestBody longq = RequestBody.create(MediaType.parse("form-data"), String.valueOf(myLng));
        RequestBody order = RequestBody.create(MediaType.parse("form-data"), String.valueOf(Oid));

        Log.e("dfdsddsds", "" + Waitingtime);
        if (!Waitingtime.isEmpty()) {

            Waitingtime1 = RequestBody.create(MediaType.parse("form-data"), String.valueOf(Waitingtime));
        } else {
            Waitingtime1 = RequestBody.create(MediaType.parse("form-data"), String.valueOf("00:00"));
        }

        ApiInterface newsFeedService = ApiClient.getClient().create(ApiInterface.class);
        Call<EndTrip> call = newsFeedService.endTrip("Token " + mPreference.getToken(), order, is_first, lat, longq, Otp, Waitingtime1, imageFileBody);
        call.enqueue(new retrofit2.Callback<EndTrip>() {

            @Override
            public void onResponse(@NonNull Call<EndTrip> call, @NonNull retrofit2.Response<EndTrip> response) {
                EndTrip endtripnew = response.body();
                if (response.isSuccessful() && endtripnew != null) {
                    Log.e("response", "" + response.toString());
                    alertDialog.dismiss();
                    mPreference.setOrderWait(0);
                    mPreference.setStartWaitingTime(null);
                    imageclock.setVisibility(View.GONE);
                    openPaymentOption();
                    hideProgressDialog();
                } else {
                    hideProgressDialog();

                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<EndTrip> call, @NonNull Throwable t) {
                hideProgressDialog();
            }
        });
    }

    private void openPaymentOption() {
        View otpView = LayoutInflater.from(mContext).inflate(R.layout.dialog_paid, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setView(otpView);
        alertDialog2 = builder.create();
        RadioGroup mGrouppayment = otpView.findViewById(R.id.grp_payment);
        RadioButton paidnew = otpView.findViewById(R.id.cash);
        RadioButton online = otpView.findViewById(R.id.online);

        final Button close_btn = otpView.findViewById(R.id.trip_closed_buton);
        //  ImageView imageViewClose = otpView.findViewById(R.id.img_close12);
        alertDialog2.setCancelable(false);

        mGrouppayment.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                PaymentMode = group.getCheckedRadioButtonId() == R.id.cash ? getString(
                        R.string.cash_received) : getString(R.string.Online_payment);
            }
        });
        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!PaymentMode.isEmpty() && PaymentMode != null) {

                    if (PaymentMode.equals("Cash")) {
                        PaymentType = "CD";

                        showProgressDialog("Processing....");
                        mPaymentOrders(Oid, new CancelOrder(4, PaymentType, ""));
                        alertDialog2.dismiss();
                    } else if (PaymentMode.equals("Online")) {
                        ToastBuilder.build(mContext, "Trip Completed");
                        mPreference.setPolyline("");
                        mPreference.setOtp("");
                        mPreference.setStartWait("");
                        mPreference.setOrderWait(0);
                        MyActivity.launchClearStack(mContext, Orders.class);
                        finish();
                        alertDialog2.dismiss();
                    }
                } else {
                    ToastBuilder.build(mContext, "Please Select Any one Option");
                }
            }
        });


        alertDialog2.show();
    }

    @Override
    public void onClick(View v) {
        if (v == locBtn) {
            getCurrentLocation();
        } else if (v == weight) {
            processStatusBtn();

        } else if (v == imgCall) {
            if (status == 2) {
                MyActivity.launchDialPad(mContext, pickMob);
            } else if (status == 7)
                MyActivity.launchDialPad(mContext, delMob);
        } else if (v == start) {
            start.setVisibility(View.GONE);
            processWaitstartBtn();


        } else if (v == path) {
            if (status == 2) {
                explicitMap(pickLat, pickLng);
            } else if (status == 7) {
                explicitMap(droplat, droplng);
            }
        }

    }

    private void mTImerclock() {
        imageclock.setVisibility(View.VISIBLE);
        mPreference.setStartWaitingTime(String.valueOf(Calendar.getInstance().getTimeInMillis()));
        imageclock.setBase(SystemClock.elapsedRealtime() + stopTime);
        imageclock.start();

    }

    private void mTImerclocknew() {
        imageclock.setVisibility(View.VISIBLE);
        long currenttime = Calendar.getInstance().getTimeInMillis();
        long timernew = Long.parseLong(mPreference.getStartWaitingtime());
        long total = timernew - currenttime;
        imageclock.setBase(SystemClock.elapsedRealtime() + total);
        imageclock.start();

    }

    private void processWaittime() {
        if (mPreference.getOrderWait() == 2 && mPreference.getOrderId() == Oid) {
            start.setVisibility(View.GONE);
            mTImerclocknew();
        } else {
            start.setVisibility(View.VISIBLE);
        }
    }

    private void explicitMap(double lat, double lng) {
        Uri uri = Uri.parse("google.navigation:q=" + lat + "," + lng);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        intent.setPackage("com.google.android.apps.maps");
        try {
            startActivity(intent);
        } catch (Exception e) {
            ToastBuilder.build(this, "map not available");
            e.printStackTrace();
        }
    }

    private void processWaitstartBtn() {
        mPreference.setStartWait(String.valueOf(Calendar.getInstance().getTimeInMillis()));
        mPreference.setOrderId(Oid);
        mTImerclock();

        if (NetworkError.getInstance(mContext).isOnline()) {
            mTimerMethod();
        } else {
            ToastBuilder.build(mContext, " No Internet Connection");
        }


    }

    private boolean verifyLocation() {
        Location location = new Location("trip");
        if (status == 7) {
            location.setLatitude(droplat);
            location.setLongitude(droplng);
        }
        return location.distanceTo(mUserLocation) < 1000;


    }

    private void getWaitingTime() {

        long endTime = Calendar.getInstance().getTimeInMillis();
        long waittime = endTime - Long.parseLong(mPreference.getStartWait());
        long second = waittime / 1000 % 60;
        long minute = waittime / (60 * 1000) % 60;
        long hour = waittime / (60 * 60 * 1000) % 24;
        mPreference.setOrderId(Oid);
        Waitingtime = String.format(Locale.getDefault(), "%02d:%02d", hour, minute);

    }

    private void processStatusBtn() {

        if (status == 2) {
            Intent i = new Intent(this, AddWeightActivity.class);
            i.putExtra("ORDER_ID", Oid);
            i.putExtra("PICK_TYPE", pickType);
            i.putExtra("PICK_LAT", mUserLocation.getLatitude());
            i.putExtra("PICK_LNG", mUserLocation.getLongitude());
            i.putExtra("DELIVER_NAME", delName);
            i.putExtra("STATUS", status);
            i.putExtra("PICK_LOC", pickAddr);
            i.putExtra("DROP_LOC", dropLoc);
            i.putExtra("DEL_LAT", droplat);
            i.putExtra("DEL_LNG", droplng);
            i.putExtra("DELIVER_NAME", delName);
            startActivityForResult(i, 1);

        } else if (status == 7) {
            openEndTripImageDialog();


        }
    }

    @SuppressWarnings("SameParameterValue")
    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        MyActivity.launchClearStack(mContext, Orders.class);
        finish();
    }

    public void mPaymentOrders(int id, CancelOrder cancelOrder) {


        Log.e("id", "" + id);
        String mUrl = ApiClient.BASE_URL + "orders/" + id + "/change_status/";
        ApiInterface authService = ApiClient.getClient().create(ApiInterface.class);
        Call<Void> call = authService.getCancelOrder(mUrl, "Token " + mPreference.getToken(), cancelOrder);

        call.enqueue(new retrofit2.Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, retrofit2.Response<Void> response) {
                if (response.isSuccessful()) {
                    hideProgressDialog();
                    alertDialog2.dismiss();
                    ToastBuilder.build(mContext, "Trip Completed");
                    mPreference.setPolyline("");
                    mPreference.setOtp("");
                    mPreference.setStartWait("");
                    mPreference.setOrderWait(0);
                    MyActivity.launchClearStack(mContext, Orders.class);
                    finish();
                } else {
                    alertDialog.dismiss();
                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }

    private void mTimerMethod() {

        myTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                getWaitingTime();
                setUrl(Waitingtime + ":00");
            }
        }, 0, 600000);
    }

    private void setUrl(String time) {
        String mUrl = ApiClient.BASE_URL + "employee/push/notification/" + Oid + "/";
        getWatingTime(mUrl, time);
    }


    private void getWatingTime(String url, String time1) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<WaitingTime> call = apiService.waitingTime("Token " + mPreference.getToken(), url, new WaitingTime(time1, true));
        call.enqueue(new Callback<WaitingTime>() {

            @Override
            public void onResponse(@NonNull Call<WaitingTime> call, @NonNull retrofit2.Response<WaitingTime> response) {
                WaitingTime profileView = response.body();
                if (response.isSuccessful() && profileView != null) {

                    Log.e("timerapi", "" + profileView.getmWaitingTime());
                    mPreference.setOrderId(Oid);
                    WaitingTimenew = profileView.getmWaitingTime();
                    mPreference.setOrderWait(2);
                    // processWaittime();
                } else {
                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<WaitingTime> call, @NonNull Throwable t) {
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }


}