package in.mrpickup.deliveryboy;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import in.mrpickup.deliveryboy.Adapter.MyOrderAdapter;
import in.mrpickup.deliveryboy.appcontroller.Api;
import in.mrpickup.deliveryboy.appcontroller.AppController;
import in.mrpickup.deliveryboy.appcontroller.PreferenceManager;
import in.mrpickup.deliveryboy.callback.OrderfeedCallback;
import in.mrpickup.deliveryboy.helper.NetworkError;
import in.mrpickup.deliveryboy.helper.Utils;
import in.mrpickup.deliveryboy.model.MyOrdersFeed;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.mrpickup.deliveryboy.appcontroller.Api.KEY_DEL_MOB;
import static in.mrpickup.deliveryboy.appcontroller.Api.KEY_STATUS_ID;
import static in.mrpickup.deliveryboy.appcontroller.Constant.TRIP_ARRAY;

@SuppressWarnings("SameParameterValue")
public class MyOrdersActivity extends AppCompatActivity implements OrderfeedCallback, SwipeRefreshLayout.OnRefreshListener {


    public ImageView mImageBack, mImageFilter;
    public Button mSearchBtn;
    protected String type, deliver_name, delMob;
    String name, categoryName;
    String storeName, payType;
    Calendar newCalendar;
    int quantity, units, subId, CatId;
    String subName, CatName, mKilometer;
    private TextView startDate, endDate;
    private ProgressDialog mProgressDialog;
    private PreferenceManager mPreference;
    private Context mContext;
    private SwipeRefreshLayout mRefreshLayout;
    private RecyclerView mRecyclerView;
    private ArrayList<MyOrdersFeed> mFeedList;
    private MyOrderAdapter mAdapter;
    private LinearLayout mnoorders;
    private Dialog dialog;
    private DateFormat dateFormatter;
    private String mStartDate, mEndDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_orders);

        iniObjects();
        initRecycler();
        initRefresh();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void iniObjects() {
        mContext = this;
        mProgressDialog = new ProgressDialog(mContext);
        mPreference = new PreferenceManager(mContext);
        mRefreshLayout = findViewById(R.id.swipe);
        mImageBack = findViewById(R.id.img_back);
        mImageFilter = findViewById(R.id.img_filter);
        mRecyclerView = findViewById(R.id.recycle);
        mFeedList = new ArrayList<>();
        mAdapter = new MyOrderAdapter(mContext, mFeedList, this);
        mnoorders = findViewById(R.id.no_order);
        mImageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mImageFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mgetFilter(mContext);
            }
        });
    }

    public void mgetFilter(final Context context) {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.order_filter);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        assert window != null;
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        startDate = dialog.findViewById(R.id.startdate);
        endDate = dialog.findViewById(R.id.endDate);
        mSearchBtn = dialog.findViewById(R.id.start_btn);
        ImageView img_close1 = dialog.findViewById(R.id.img_close1);

        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        newCalendar = Calendar.getInstance();
        dialog.show();

        startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DatePickerDialog fromDatePickerDialog = new android.app.DatePickerDialog(context, R.style.DatePickerTheme

                        , new android.app.DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        String start = dateFormatter.format(newDate.getTime());
                        startDate.setText(dateFormatter.format(newDate.getTime()));
                    }
                }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
                fromDatePickerDialog.show();
                fromDatePickerDialog.getDatePicker().setBackgroundColor(ContextCompat.getColor(mContext, R.color.white));

            }

        });

        endDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("calendaer", "" + newCalendar.getTime());
                DatePickerDialog toDatePickerDialog = new android.app.DatePickerDialog(context, R.style.DatePickerTheme, new android.app.DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        endDate.setText(dateFormatter.format(newDate.getTime()));
                    }

                }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
                toDatePickerDialog.show();
                toDatePickerDialog.getDatePicker().setBackgroundColor(ContextCompat.getColor(mContext, R.color.white));

            }
        });


        img_close1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        mSearchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mStartDate = startDate.getText().toString();
                mEndDate = endDate.getText().toString();

                if (mStartDate.isEmpty()) {
                    ToastBuilder.build(mContext, "Please Select the StartDate");
                } else if (mEndDate.isEmpty()) {
                    ToastBuilder.build(mContext, "Please Select the EndDate");
                } else {
                    getData1(mStartDate, mEndDate);
                    dialog.dismiss();

                }

            }
        });

    }

    private void initRecycler() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mRecyclerView.setAdapter(mAdapter);

    }

    private void initRefresh() {
        mRefreshLayout.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark,
                R.color.colorAccent);
        mRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mRefreshLayout.setRefreshing(true);

                if (NetworkError.getInstance(mContext).isOnline()) {
                    getData();
                } else {
                    ToastBuilder.build(mContext, " No Internet Connection");
                }


            }
        });
    }

    @Override
    public void onItemClick(int position) {
        MyOrdersFeed feeds = mFeedList.get(position);
        int orderId = feeds.getmId();
        int status = feeds.getmStatus();
        double srcLat = feeds.getmStartlat();
        double srcLng = feeds.getmStartlng();
        double endlat = feeds.getmEndlat();
        double endlng = feeds.getmEndlng();
        String name = feeds.getmName();
        String pickLoc = feeds.getmPickLoc();
        String pickMobile = feeds.getmMob();
        String dropLoc = feeds.getmDropLoc();
        String date = feeds.getmDate();
        String category = feeds.getmCategory();
        boolean pickType = feeds.getmPickType();

        String delName = feeds.getmDelname();
        String description = feeds.getmDescription();
        int orderId1 = feeds.getmId();
        Intent intent = new Intent(getBaseContext(), MyOrdersDetailActivity.class);
        intent.putExtra("SOURCE_LAT", srcLat);
        intent.putExtra("SOURCE_LNG", srcLng);
        intent.putExtra("NAME", name);
        intent.putExtra("PICK_LOC", pickLoc);
        intent.putExtra("DROP_LOC", dropLoc);
        intent.putExtra("ORDER_ID", orderId1);
        intent.putExtra("PICK_MOB", pickMobile);
        intent.putExtra("DELIVERY_MOB", feeds.getmDelmob());
        intent.putExtra("PICK_TYPE", pickType);
        intent.putExtra("STATUS", status);
        intent.putExtra("DELIVER_NAME", feeds.getmDelname());
        intent.putExtra("DEL_LAT", endlat);
        intent.putExtra("DEL_LNG", endlng);
        intent.putExtra("Date", date);
        intent.putExtra("category", category);
        intent.putExtra("DELNAME", delName);
        intent.putExtra("Description", description);
        intent.putExtra("StoreName", feeds.getmStorename());
        intent.putExtra("PaymentType", feeds.getmPaytype());
        intent.putExtra("ordertype", feeds.getmOrderType());
        intent.putExtra("weight", feeds.getmWeight());
        intent.putExtra("kilometer", feeds.getmKilometer());
        intent.putExtra("ordershow", feeds.getmOrderShow());
        intent.putParcelableArrayListExtra(TRIP_ARRAY, mFeedList);
        startActivity(intent);
    }

    @Override
    public void onGoclick(int position) {

    }

    @Override
    public void onPcallclick(int position) {

    }

    @Override
    public void onviewInfoclick(int position) {

    }

    @Override
    public void onDcalclick(int position) {

    }

    @Override
    public void onRefresh() {

        if (NetworkError.getInstance(mContext).isOnline()) {
            getData();
        } else {
            ToastBuilder.build(mContext, " No Internet Connection");
        }

    }

    private void getData() {

        showProgressDialog("Loading...");
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                Api.MY_ORDERS, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                hideProgressDialog();

                mnoorders.setVisibility(View.GONE);
                handleOrdersList(response);
                if (mFeedList.isEmpty()) {
                    mRefreshLayout.setRefreshing(false);
                    hideProgressDialog();
                    mnoorders.setVisibility(View.VISIBLE);
                    ToastBuilder.build(mContext, "No Orders Yet");

                } else {
                    hideProgressDialog();
                    mRefreshLayout.setRefreshing(false);
                    mnoorders.setVisibility(View.GONE);

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                mRefreshLayout.setRefreshing(false);
                VolleyErrorHandler.processError(mContext, hashCode(), String.valueOf(error));

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Token " + mPreference.getToken());

                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonArrayRequest);
    }

    private void handleOrdersList(JSONArray response) {
        mFeedList.clear();
        try {
            for (int i = 0; i < response.length(); i++) {
                JSONObject jsonObj = response.getJSONObject(i);
                JSONObject orderObj = jsonObj.getJSONObject(Api.KEY_ORDER);

                JSONArray userObj = orderObj.getJSONArray(Api.KEY_ORDERASSIGNMENT);
                for (int j = 0; j < userObj.length(); j++) {
                    JSONObject userObj22 = userObj.getJSONObject(j);
                    JSONObject KeyNameObj = userObj22.getJSONObject(Api.KEY_ASSIGNEMP);
                }

                int id = orderObj.getInt(Api.KEY_ID);
                String orderId = orderObj.getString(Api.KEY_ORDERIDSHOW);
                String pickLoc = orderObj.getString(Api.KEY_PICKUP_LOC);
                double pickLat = orderObj.getDouble(Api.KEY_PICKUP_LAt);
                String pickMob = orderObj.getString(Api.KEY_PICK_MOB);
                name = orderObj.getString(Api.KEY_PICKUPNAME);
                storeName = orderObj.getString("store_name");
                delMob = orderObj.getString(KEY_DEL_MOB);
                double pickLng = orderObj.getDouble(Api.KEY_PICKUP_LNG);
                String delLoc = orderObj.getString(Api.KEY_DELIVER_LOC);
                double delLat = orderObj.getDouble(Api.KEY_DELIVER_LAT);
                double delLng = orderObj.getDouble(Api.KEY_DELIVER_LONG);
                boolean pickType = orderObj.getBoolean(Api.KEY_PICKUP_TYPE);
                String createdOn = Utils.getRelativeTime(orderObj.getString(Api.KEY_CREATED_ON));
                int cId = orderObj.getInt(Api.KEY_ID);
                String Description = orderObj.getString("descriptions");

                boolean delType = orderObj.getBoolean(Api.KEY_DELIVERYTYPE);
                if (delType)
                    type = "Pickup and Deliver";
                else type = "Buy and Deliver";
                payType = orderObj.getString(Api.KEY_PAYMENT);
                JSONObject detailObj = orderObj.getJSONObject("orderdetails");
                String dType = orderObj.getString("delivery_type");
                String pWeight = orderObj.getString("actual_weight");
                double Rating = orderObj.getDouble("rating");
                String comments = orderObj.getString("comments");

                JSONObject statusObj = detailObj.getJSONObject(Api.KEY_STATUS);
                mKilometer = detailObj.getString("approx_dist");
                int status = statusObj.getInt(KEY_STATUS_ID);
                deliver_name = orderObj.getString("del_name");
                JSONArray productObj = orderObj.getJSONArray("categories");
                for (int k = 0; k < productObj.length(); k++) {
                    JSONObject ProductnameObj = productObj.getJSONObject(k);
                    for (int m = 0; m < ProductnameObj.length(); m++) {
                        JSONArray jsonArray = ProductnameObj.getJSONArray("products");
                        for (int y = 0; y < jsonArray.length(); y++) {
                            JSONObject ProductnameObj1 = jsonArray.getJSONObject(y);
                            quantity = ProductnameObj1.getInt("units");
                            units = ProductnameObj1.getInt("quantity");
                            subId = ProductnameObj1.getInt("id");
                            subName = ProductnameObj1.getString("name");
                        }
                    }
                    JSONObject jsonObject1 = ProductnameObj.getJSONObject("category");
                    categoryName = jsonObject1.getString("name");
                    CatId = jsonObject1.getInt("id");

                    mFeedList.add(new MyOrdersFeed(id, name, pickMob, pickLoc, delLoc, pickLat, pickLng, delLat, delLng,
                            pickType, cId, categoryName, type, payType, createdOn, status, dType, pWeight,
                            subName, mKilometer, delMob, deliver_name, storeName, payType, Rating,
                            comments, String.valueOf(units), Description, orderId));
                }


            }
            mAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            Log.e("lt", response.toString());
            Log.e("dfdfd", e.toString());
            e.printStackTrace();
        }
    }


    private void getData1(String sDate, String eDate) {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                Api.MY_ORDERSFILTER + sDate + "&end_date=" + eDate, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                mRefreshLayout.setRefreshing(false);
                handleOrdersList1(response);
                if (mFeedList.isEmpty()) {
                    mnoorders.setVisibility(View.VISIBLE);
                    ToastBuilder.build(mContext, "No Orders in This Date");

                } else {
                    mnoorders.setVisibility(View.GONE);

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                mRefreshLayout.setRefreshing(false);
                VolleyErrorHandler.processError(mContext, hashCode(), String.valueOf(error));

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Token " + mPreference.getToken());

                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonArrayRequest);
    }

    private void handleOrdersList1(JSONArray response) {
        mFeedList.clear();
        try {
            for (int i = 0; i < response.length(); i++) {
                JSONObject jsonObj = response.getJSONObject(i);
                JSONObject orderObj = jsonObj.getJSONObject(Api.KEY_ORDER);

                JSONArray userObj = orderObj.getJSONArray(Api.KEY_ORDERASSIGNMENT);
                for (int j = 0; j < userObj.length(); j++) {
                    JSONObject userObj22 = userObj.getJSONObject(j);
                    JSONObject KeyNameObj = userObj22.getJSONObject(Api.KEY_ASSIGNEMP);
                }

                int id = orderObj.getInt(Api.KEY_ID);
                String pickLoc = orderObj.getString(Api.KEY_PICKUP_LOC);
                double pickLat = orderObj.getDouble(Api.KEY_PICKUP_LAt);
                String orderId = orderObj.getString(Api.KEY_ORDERIDSHOW);
                String pickMob = orderObj.getString(Api.KEY_PICK_MOB);
                name = orderObj.getString(Api.KEY_PICKUPNAME);
                storeName = orderObj.getString("store_name");
                delMob = orderObj.getString(KEY_DEL_MOB);
                double pickLng = orderObj.getDouble(Api.KEY_PICKUP_LNG);
                String delLoc = orderObj.getString(Api.KEY_DELIVER_LOC);
                double delLat = orderObj.getDouble(Api.KEY_DELIVER_LAT);
                double delLng = orderObj.getDouble(Api.KEY_DELIVER_LONG);
                boolean pickType = orderObj.getBoolean(Api.KEY_PICKUP_TYPE);
                String createdOn = Utils.getRelativeTime(orderObj.getString(Api.KEY_CREATED_ON));
                int cId = orderObj.getInt(Api.KEY_ID);

                JSONArray productObj = orderObj.getJSONArray("categories");
                for (int k = 0; k < productObj.length(); k++) {
                    JSONObject ProductnameObj = productObj.getJSONObject(k);
                    JSONArray jsonArray = ProductnameObj.getJSONArray("products");
                    JSONObject jsonObject1 = ProductnameObj.getJSONObject("category");
                    categoryName = jsonObject1.getString("name");
                    CatId = jsonObject1.getInt("id");
                    Log.e("catname", "" + jsonObject1.getString("name"));
                    for (int y = 0; y < jsonArray.length(); y++) {
                        JSONObject ProductnameObj1 = jsonArray.getJSONObject(y);
                        quantity = ProductnameObj1.getInt("units");
                        units = ProductnameObj1.getInt("quantity");
                        subId = ProductnameObj1.getInt("id");
                        subName = ProductnameObj1.getString("name");
                    }

                }
                String Description = orderObj.getString("descriptions");

                boolean delType = orderObj.getBoolean(Api.KEY_DELIVERYTYPE);
                if (delType)
                    type = "Pickup and Deliver";
                else type = "Buy and Deliver";
                payType = orderObj.getString(Api.KEY_PAYMENT);
                JSONObject detailObj = orderObj.getJSONObject("orderdetails");
                String dType = orderObj.getString("delivery_type");
                String pWeight = orderObj.getString("actual_weight");
                JSONObject statusObj = detailObj.getJSONObject(Api.KEY_STATUS);
                double Rating = orderObj.getDouble("rating");
                String comments = orderObj.getString("comments");
                mKilometer = detailObj.getString("approx_dist");
                int status = statusObj.getInt(KEY_STATUS_ID);
                deliver_name = orderObj.getString("del_name");
                mFeedList.add(new MyOrdersFeed(id, name, pickMob, pickLoc, delLoc, pickLat, pickLng, delLat, delLng,
                        pickType, cId, categoryName, type, payType, createdOn, status, dType, pWeight,
                        subName, mKilometer, delMob, deliver_name, storeName, payType, Rating,
                        comments, String.valueOf(units), Description, orderId));
                Log.e("mFeedList", "" + mFeedList.size());
            }
            mAdapter.notifyDataSetChanged();
        } catch (JSONException e) {

            e.printStackTrace();
        }
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

}
