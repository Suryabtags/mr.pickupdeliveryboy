package in.mrpickup.deliveryboy.helper;

import android.text.format.DateUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Admin on 7/6/2017.
 */

public class Utils {


    public static String getRelativeTime(String createdDate) {
        try {
            Date dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS",
                    Locale.getDefault()).parse(createdDate);
            return DateUtils.getRelativeTimeSpanString(dateFormat.getTime(),
                    System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS,
                    DateUtils.FORMAT_ABBREV_RELATIVE).toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return createdDate;
    }

    public static String getSeconds(String string) {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());

        long sec = -1;
        try {
            Date reference = dateFormat.parse("00:00:00");
            Date date = dateFormat.parse(string);
            sec = (date.getTime() - reference.getTime()) / 1000L;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (sec < 60) {
            return sec + " s";
        } else if (sec >= 60 && sec < 3600) {
            sec = sec / 60;
            return sec + (sec == 1 ? " min" : " mins");
        } else {
            long hour = sec / 3600;
            long minutes = (sec / 60) % 60;
            long seconds = sec % 60;
            if (hour == 0 && minutes == 0) {
                return seconds + "s";
            } else if (hour == 0) {
                return minutes + (minutes == 1 ? " min" : " mins") + (seconds == 0 ? ""
                        : seconds + " s");
            } else {
                return hour + (hour == 1 ? " hr " : " hrs ") + minutes + (minutes == 1 ? " min"
                        : " mins") + (seconds == 0 ? "" : seconds + " s");
            }

        }
    }
}