package in.mrpickup.deliveryboy;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 10/6/2017.
 */

public class latlongSend {

    @SerializedName("latitude")
    private String latitude;
    @SerializedName("longitude")
    private String longitude;
    @SerializedName("on_leave")
    private boolean omOnleave;
    @SerializedName("useraddress")
    private UserAddress userAddress;

    public latlongSend(String mLatitude, String mLongitude, boolean mleave) {
        latitude = mLatitude;
        longitude = mLongitude;
        omOnleave = mleave;
    }

    public UserAddress getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(UserAddress userAddress) {
        this.userAddress = userAddress;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }


    public boolean isOmOnleave() {
        return omOnleave;
    }

    public void setOmOnleave(boolean omOnleave) {
        this.omOnleave = omOnleave;
    }


    public class UserAddress {

        @SerializedName("id")
        @Expose
        private String id;

        @SerializedName("street")
        @Expose
        private String street;
        @SerializedName("city")
        private String city;
        @SerializedName("state")
        @Expose
        private String state;
        @SerializedName("country")
        private String country;

        @SerializedName("area")
        private String zipcode;
        @SerializedName("long")
        private String longitude;

        @SerializedName("lat")
        private String latitude;


        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getStreet() {
            return street;
        }

        public void setStreet(String street) {
            this.street = street;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getZipcode() {
            return zipcode;
        }

        public void setZipcode(String zipcode) {
            this.zipcode = zipcode;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }


    }
}
