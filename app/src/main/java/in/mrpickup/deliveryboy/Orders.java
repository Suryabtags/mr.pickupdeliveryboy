package in.mrpickup.deliveryboy;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.kyleduo.switchbutton.SwitchButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.mrpickup.deliveryboy.Adapter.OrderFeedAdapter;
import in.mrpickup.deliveryboy.appcontroller.Api;
import in.mrpickup.deliveryboy.appcontroller.ApiClient;
import in.mrpickup.deliveryboy.appcontroller.ApiInterface;
import in.mrpickup.deliveryboy.appcontroller.AppController;
import in.mrpickup.deliveryboy.appcontroller.ErrorHandler;
import in.mrpickup.deliveryboy.appcontroller.MyActivity;
import in.mrpickup.deliveryboy.appcontroller.PreferenceManager;
import in.mrpickup.deliveryboy.callback.OrderfeedCallback;
import in.mrpickup.deliveryboy.helper.NetworkError;
import in.mrpickup.deliveryboy.helper.Utils;
import in.mrpickup.deliveryboy.model.FcmToken;
import in.mrpickup.deliveryboy.model.Feed;
import in.mrpickup.deliveryboy.model.UpdateLeave;
import in.mrpickup.deliveryboy.model.UpdateLeaveResponse;
import retrofit2.Call;
import retrofit2.Callback;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.mrpickup.deliveryboy.appcontroller.Api.KEY_DEL_MOB;
import static in.mrpickup.deliveryboy.appcontroller.Api.KEY_STATUS_ID;
import static in.mrpickup.deliveryboy.appcontroller.Constant.TRIP_ARRAY;
import static in.mrpickup.deliveryboy.appcontroller.MyActivity.launch;
import static in.mrpickup.deliveryboy.helper.Macaddress.getMacAddress;

public class Orders extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, OrderfeedCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    final static int REQUEST_LOCATION = 199;
    private static final String EXTRA_ID = "id";
    private static final String TAG = Orders.class.getSimpleName();
    private static final int RESULT_CODE = 143;
    public final int UPDATE_INTERVAL = 300000;
    public final int FASTEST_INTERVAL = 300000;
    private final int REQ_PERMISSION = 999;
    public LocationRequest locationRequest;
    protected String type, deliver_name, delMob;
    AlertDialog dailog;
    Dialog alertDialog;
    AlertDialog.Builder build;
    String name, categoryName;
    String storeName, payType;
    SwitchButton mOnLeave;
    boolean onleave;
    AlertDialog.Builder builder;
    String delname;
    int quantity, units, subId, CatId;
    String subName, CatName, mKilometer;
    private Toolbar mToolbar;
    private ProgressDialog mProgressDialog;
    private PreferenceManager mPreference;
    private Context mContext;
    private SwipeRefreshLayout mRefreshLayout;
    private RecyclerView mRecyclerView;
    private ArrayList<Feed> mFeedList;
    private OrderFeedAdapter mAdapter;
    private LinearLayout mnoorders;
    private Menu menu;
    private GoogleApiClient googleApiClient;
    private Location lastLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);
        initObjects();
        initCallbacks();
        setupToolbar();
        initRecycler();
        initRefresh();
        //fcmToken();


        if (NetworkError.getInstance(mContext).isOnline()) {
            updateFcm();
        } else {
            ToastBuilder.build(mContext, " No Internet Connection");
        }
        trackLocation();
        createGoogleApi();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();

        mRefreshLayout.setRefreshing(true);

        if (NetworkError.getInstance(mContext).isOnline()) {
            getData();
        } else {
            ToastBuilder.build(mContext, " No Internet Connection");
        }
    }

    private void initObjects() {

        mContext = this;
        build = new AlertDialog.Builder(mContext); // connectivity
        mToolbar = findViewById(R.id.toolbar);
        mProgressDialog = new ProgressDialog(this);
        mPreference = new PreferenceManager(this);
        mRefreshLayout = findViewById(R.id.swipe);


        mRecyclerView = findViewById(R.id.recycle);


        mFeedList = new ArrayList<>();
        mAdapter = new OrderFeedAdapter(mContext, mFeedList, this);
        mnoorders = findViewById(R.id.no_order);

        // create GoogleApiClient


    }

    private void initCallbacks() {
        mRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.nav_prof, menu);
        this.menu = menu;
        return true;
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setTitle(null);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        int i = 0;
        switch (item.getItemId()) {
            case R.id.nav_onLeave:

                if (NetworkError.getInstance(this).isOnline()) {
                    openOnLeaveDialog();
                } else {
                    ToastBuilder.build(this, " No Internet Connection");
                }

                return true;
            case R.id.nav_logout:
                if (NetworkError.getInstance(this).isOnline()) {
                    logoutDialog(item);
                } else {
                    ToastBuilder.build(this, " No Internet Connection");
                }

                return true;
            case R.id.nav_Profile:
                launch(mContext, ProfileNew.class);
                return true;
            case R.id.nav_MyOrders:
                launch(mContext, MyOrdersActivity.class);
                return true;
            case R.id.nav_call:
                calling();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void openOnLeaveDialog() {
        View onLeaveView = LayoutInflater.from(mContext).inflate(R.layout.dialog_leave, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setView(onLeaveView);
        final AlertDialog alertDialog = builder.create();

        mOnLeave = onLeaveView.findViewById(R.id.onleave_switch);
        TextView txt_onleave = onLeaveView.findViewById(R.id.txt_onleave);


        ImageView imageViewClose = onLeaveView.findViewById(R.id.img_close1);
        imageViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        mOnLeave.setChecked(onleave);
        mOnLeave.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    UpdateLeave(isChecked);

                } else {
                    UpdateLeave(false);
                }
            }
        });

        alertDialog.show();
    }

    public void calling() {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:+91 8344114411"));
        startActivity(callIntent);
    }

    private void initRecycler() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mRecyclerView.setAdapter(mAdapter);

    }

    private void initRefresh() {
        mRefreshLayout.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark,
                R.color.colorAccent);
        mRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mRefreshLayout.setRefreshing(true);

                if (NetworkError.getInstance(mContext).isOnline()) {
                    getData();
                } else {
                    ToastBuilder.build(mContext, " No Internet Connection");
                }

            }
        });
    }

    private void getData() {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                Api.LIST_ORDER, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                mRefreshLayout.setRefreshing(false);
                handleOrdersList(response);
                if (mFeedList.isEmpty()) {
                    mRefreshLayout.setRefreshing(false);
                    mnoorders.setVisibility(View.VISIBLE);
                    ToastBuilder.build(mContext, "No Orders Yet");

                } else {
                    mnoorders.setVisibility(View.GONE);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                mRefreshLayout.setRefreshing(false);
                VolleyErrorHandler.processError(mContext, hashCode(), String.valueOf(error));

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Token " + mPreference.getToken());

                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonArrayRequest);
    }

    private void handleOrdersList(JSONArray response) {
        mFeedList.clear();

        try {
            for (int i = 0; i < response.length(); i++) {
                JSONObject jsonObj = response.getJSONObject(i);
                JSONObject orderObj = jsonObj.getJSONObject(Api.KEY_ORDER);
                JSONArray userObj = orderObj.getJSONArray(Api.KEY_ORDERASSIGNMENT);
                for (int j = 0; j < userObj.length(); j++) {
                    JSONObject userObj22 = userObj.getJSONObject(j);
                    JSONObject KeyNameObj = userObj22.getJSONObject(Api.KEY_ASSIGNEMP);
                }

                int id = orderObj.getInt(Api.KEY_ID);
                String orderId = orderObj.getString(Api.KEY_ORDERIDSHOW);
                String pickLoc = orderObj.getString(Api.KEY_PICKUP_LOC);
                double pickLat = orderObj.getDouble(Api.KEY_PICKUP_LAt);
                String pickMob = orderObj.getString(Api.KEY_PICK_MOB);
                name = orderObj.getString(Api.KEY_PICKUPNAME);
                delname = orderObj.getString(Api.KEY_DELNAME);
                storeName = orderObj.getString("store_name");

                String Description = orderObj.getString("descriptions");
                delMob = orderObj.getString(KEY_DEL_MOB);
                double pickLng = orderObj.getDouble(Api.KEY_PICKUP_LNG);
                String delLoc = orderObj.getString(Api.KEY_DELIVER_LOC);
                double delLat = orderObj.getDouble(Api.KEY_DELIVER_LAT);
                double delLng = orderObj.getDouble(Api.KEY_DELIVER_LONG);
                boolean pickType = orderObj.getBoolean(Api.KEY_PICKUP_TYPE);
                String createdOn = Utils.getRelativeTime(orderObj.getString(Api.KEY_CREATED_ON));
                int cId = orderObj.getInt(Api.KEY_ID);
                JSONArray productObj = orderObj.getJSONArray("categories");
                for (int k = 0; k < productObj.length(); k++) {
                    JSONObject ProductnameObj = productObj.getJSONObject(k);

                    for (int m = 0; m < ProductnameObj.length(); m++) {
                        JSONArray jsonArray = ProductnameObj.getJSONArray("products");
                        for (int y = 0; y < jsonArray.length(); y++) {
                            JSONObject ProductnameObj1 = jsonArray.getJSONObject(y);
                            quantity = ProductnameObj1.getInt("units");
                            units = ProductnameObj1.getInt("quantity");
                            subId = ProductnameObj1.getInt("id");
                            subName = ProductnameObj1.getString("name");
                        }
                    }

                    JSONObject jsonObject1 = ProductnameObj.getJSONObject("category");
                    categoryName = jsonObject1.getString("name");
                    CatId = jsonObject1.getInt("id");
                }
                boolean delType = orderObj.getBoolean(Api.KEY_DELIVERYTYPE);
                if (delType)
                    type = "Pickup and Deliver";
                else type = "Buy and Deliver";
                payType = orderObj.getString(Api.KEY_PAYMENT);
                JSONObject detailObj = orderObj.getJSONObject("orderdetails");
                String dType = orderObj.getString("delivery_type");
                String pWeight = orderObj.getString("product_weight");

                JSONObject statusObj = detailObj.getJSONObject(Api.KEY_STATUS);
                int status = statusObj.getInt(KEY_STATUS_ID);
                mFeedList.add(new Feed(id, name, pickMob, pickLoc, delLoc, pickLat, pickLng, delLat, delLng,
                        pickType, cId, categoryName, type, payType, createdOn, status, delname,
                        storeName, delMob, dType, subName, pWeight, String.valueOf(units), Description, orderId));

            }
            mAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            Log.e("lt", response.toString());
            Log.e("dfdfd", e.toString());
            e.printStackTrace();
        }
    }

    public void logoutDialog(MenuItem item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage("Are you sure you want to Logout?");
        builder.setPositiveButton("Logout", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                showProgressDialog("Logging out..");
                logoutUser();
                onStop();

            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void logoutUser() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.LOGOUT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        handleLogoutResponse();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                VolleyErrorHandler.handle(mContext, error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders()
                    throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Token " + mPreference.getToken());
                Log.e("headers123", "" + headers);
                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void handleLogoutResponse() {
        mPreference.clearUser();
        ToastBuilder.build(mContext, "You\'ve logged out");
        MyActivity.launchClearStack(mContext, Splash.class);
    }

    @SuppressWarnings("SameParameterValue")
    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onRefresh() {

        if (NetworkError.getInstance(mContext).isOnline()) {
            getData();
        } else {
            ToastBuilder.build(mContext, " No Internet Connection");
        }


    }

    @Override
    public void onItemClick(final int position) {
        Feed feeds = mFeedList.get(position);
        int orderId = feeds.getmId();
        int status = feeds.getmStatus();
        double srcLat = feeds.getmStartlat();
        double srcLng = feeds.getmStartlng();
        double endlat = feeds.getmEndlat();
        double endlng = feeds.getmEndlng();
        String name = feeds.getmName();
        String pickLoc = feeds.getmPickLoc();
        String pickMobile = feeds.getmMob();
        String dropLoc = feeds.getmDropLoc();
        String date = feeds.getmDate();
        String category = feeds.getmCategory();
        boolean pickType = feeds.getmPickType();
        String paytype = feeds.getmPayType();
        String dname = feeds.getmDelName();
        String Storename = feeds.getmStorename();
        String dmob = feeds.getmDMob();
        String delName = feeds.getmDelName();
        String description = feeds.getmDescriptions();

        if (status == 2) {
            int orderId1 = feeds.getmId();
            Intent intent = new Intent(getBaseContext(), OrderDetailsActivity.class);
            intent.putExtra("SOURCE_LAT", srcLat);
            intent.putExtra("SOURCE_LNG", srcLng);
            intent.putExtra("NAME", name);
            intent.putExtra("DELNAME", delName);
            intent.putExtra("PICK_LOC", pickLoc);
            intent.putExtra("DROP_LOC", dropLoc);
            intent.putExtra("ORDER_ID", orderId1);
            intent.putExtra("PICK_MOB", pickMobile);
            intent.putExtra("DELIVERY_MOB", dmob);
            intent.putExtra("PICK_TYPE", pickType);
            intent.putExtra("STATUS", status);
            intent.putExtra("DELIVER_NAME", dname);
            intent.putExtra("DEL_LAT", endlat);
            intent.putExtra("DEL_LNG", endlng);
            intent.putExtra("Date", date);
            intent.putExtra("category", category);
            intent.putExtra("StoreName", Storename);
            intent.putExtra("PaymentType", paytype);
            intent.putExtra("Description", description);
            intent.putExtra("ordertype", feeds.getmDtype());
            intent.putExtra("weight", feeds.getmWeight());
            intent.putExtra("ordershow", feeds.getmOrderIDShow());

            intent.putParcelableArrayListExtra(TRIP_ARRAY, mFeedList);
            startActivity(intent);
        } else if (status == 7) {
            Intent intent = new Intent(getBaseContext(), MapActivity.class);
            intent.putExtra("SOURCE_LAT", srcLat);
            intent.putExtra("SOURCE_LNG", srcLng);
            intent.putExtra("NAME", name);
            intent.putExtra("PICK_LOC", pickLoc);
            intent.putExtra("DROP_LOC", dropLoc);
            intent.putExtra("ORDER_ID", orderId);
            intent.putExtra("PICK_MOB", pickMobile);
            intent.putExtra("DELIVERY_MOB", dmob);
            intent.putExtra("PICK_TYPE", pickType);
            intent.putExtra("STATUS", status);
            intent.putExtra("DELIVER_NAME", dname);
            intent.putExtra("DEL_LAT", endlat);
            intent.putExtra("DEL_LNG", endlng);
            intent.putExtra("ordershow", feeds.getmOrderIDShow());
            startActivityForResult(intent, RESULT_CODE);
        }
    }

    @Override
    public void onGoclick(int position) {

    }

    @Override
    public void onPcallclick(int position) {

    }

    @Override
    public void onviewInfoclick(int position) {
        Feed feeds = mFeedList.get(position);
        int orderId = feeds.getmId();
        int status = feeds.getmStatus();
        double srcLat = feeds.getmStartlat();
        double srcLng = feeds.getmStartlng();
        double endlat = feeds.getmEndlat();
        double endlng = feeds.getmEndlng();
        String name = feeds.getmName();
        String pickLoc = feeds.getmPickLoc();
        String pickMobile = feeds.getmMob();
        String dropLoc = feeds.getmDropLoc();
        String date = feeds.getmDate();
        String category = feeds.getmCategory();
        boolean pickType = feeds.getmPickType();
        String paytype = feeds.getmPayType();
        String dname = feeds.getmDelName();
        String Storename = feeds.getmStorename();
        String dmob = feeds.getmDMob();
        Intent intent = new Intent(getBaseContext(), ViewInfoDetailsActivity.class);
        intent.putExtra("SOURCE_LAT", srcLat);
        intent.putExtra("SOURCE_LNG", srcLng);
        intent.putExtra("NAME", name);
        intent.putExtra("PICK_LOC", pickLoc);
        intent.putExtra("DROP_LOC", dropLoc);
        intent.putExtra("ORDER_ID", orderId);
        intent.putExtra("PICK_MOB", pickMobile);
        intent.putExtra("DELIVERY_MOB", dmob);
        intent.putExtra("PICK_TYPE", pickType);
        intent.putExtra("STATUS", status);
        intent.putExtra("DELIVER_NAME", dname);
        intent.putExtra("DEL_LAT", endlat);
        intent.putExtra("DEL_LNG", endlng);
        intent.putExtra("Date", date);
        intent.putExtra("category", category);
        intent.putExtra("StoreName", Storename);
        intent.putExtra("PaymentType", paytype);
        intent.putExtra("ordertype", feeds.getmDtype());
        intent.putExtra("weight", feeds.getmWeight());

        intent.putParcelableArrayListExtra(TRIP_ARRAY, mFeedList);
        startActivity(intent);
    }

    @Override
    public void onDcalclick(int position) {

    }

    private void updateFcm() {
        Log.e("fcmToken", "" + mPreference.getFcmToken());
        if (mPreference.isTokenUploaded() || mPreference.getFcmToken() == null)
            return;

        ApiInterface authService = ApiClient.getClient().create(ApiInterface.class);
        Call<Void> call = authService.updateFcm("Token " + mPreference.getToken(),
                new FcmToken(getMacAddress(), mPreference.getFcmToken()));
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull retrofit2.Response<Void> response) {
                if (response.isSuccessful()) {
                    mPreference.setTokenUploaded(true);
                } else {
                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
                Log.e("fcmerror", "" + t.getMessage());
            }
        });
    }

    private void UpdateLeave(boolean status) {
        ApiInterface authService = ApiClient.getClient().create(ApiInterface.class);
        Call<UpdateLeaveResponse> call = authService.updateLeave("Token " + mPreference.getToken(),
                new UpdateLeave(status));
        call.enqueue(new Callback<UpdateLeaveResponse>() {
            @Override
            public void onResponse(@NonNull Call<UpdateLeaveResponse> call, @NonNull retrofit2.Response<UpdateLeaveResponse> response) {

                UpdateLeaveResponse updateLeaveResponse = response.body();
                if (response.isSuccessful() && updateLeaveResponse != null) {
                    onleave = updateLeaveResponse.getUserprofile().ismOnLeave();
                    Log.e("onleave", "" + onleave);
                } else {
                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<UpdateLeaveResponse> call, @NonNull Throwable t) {
                Log.e("onleaveerror", "" + t.getMessage());
            }
        });
    }


    private boolean checkIfGooglePlayEnabled() {
        if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) == ConnectionResult.SUCCESS) {
            return true;
        } else {
            Log.e("orderclass", "unable to connect to google play services.");
            Toast.makeText(getApplicationContext(), R.string.google_play_services_unavailable, Toast.LENGTH_LONG).show();
            return false;
        }
    }

    private void createGoogleApi() {
        Log.d("orders", "createGoogleApi()");
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Call GoogleApiClient connection when starting the Activity
        googleApiClient.connect();
    }

    @Override
    protected void onStop() {


        // Disconnect GoogleApiClient when stopping Activity
        googleApiClient.disconnect();
        super.onStop();
    }

    private boolean checkPermission() {
        Log.d("orders", "checkPermission()");
        // Ask for permission if it wasn't granted yet
        return (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED);
    }

    // Asks for permission
    private void askPermission() {
        Log.d("orders", "askPermission()");
        ActivityCompat.requestPermissions(
                this,
                new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                REQ_PERMISSION
        );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d("oders", "onRequestPermissionsResult()");
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQ_PERMISSION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission granted
                    getLastKnownLocation();

                } else {
                    // Permission denied
                    permissionsDenied();
                }
                break;
            }
        }
    }

    // Start location Updates
    private void startLocationUpdates() {
        Log.i("orders", "startLocationUpdates()");
        locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);

        if (checkPermission())
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    private void permissionsDenied() {
        Log.w("oders", "permissionsDenied()");
        // TODO close app and warn user
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i("orders", "onConnected()");
        getLastKnownLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("orders", "onLocationChanged [" + location + "]");
        lastLocation = location;
        writeActualLocation(location);
    }


    private void getLastKnownLocation() {
        Log.d(TAG, "getLastKnownLocation()");
        if (checkPermission()) {
            lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            if (lastLocation != null) {
                Log.i(TAG, "LasKnown location. " +
                        "Long: " + lastLocation.getLongitude() +
                        " | Lat: " + lastLocation.getLatitude());
                startLocationUpdates();
            } else {
                Log.w(TAG, "No location retrieved yet");
                startLocationUpdates();
            }
        } else askPermission();
    }

    private void writeActualLocation(Location location) {

        double lat = location.getLatitude();
        double lon = location.getLongitude();
        String lat1 = Double.toString(lat);
        String long1 = Double.toString(lon);
        sendLocationDataToWebsite(new latlongSend(lat1, long1, false));

    }


    public void sendLocationDataToWebsite(latlongSend latlong) {
        ApiInterface authService = ApiClient.getClient().create(ApiInterface.class);
        Log.e("retrofit", "" + mPreference.getToken());
        Call<latlongSend> call = authService.getlatlong("Token " + mPreference.getToken(), latlong);

        call.enqueue(new retrofit2.Callback<latlongSend>() {

            @Override
            public void onResponse(@NonNull Call<latlongSend> call, @NonNull retrofit2.Response<latlongSend> response) {

                latlongSend lat = response.body();
                if (response.isSuccessful() && lat != null) {
                    Log.e("sucesstoken", "" + lat.getUserAddress().getLatitude());
                    Log.e("sucesstoken", "" + lat.getUserAddress().getLongitude());
                } else {

                }

            }

            @Override
            public void onFailure(@NonNull Call<latlongSend> call, @NonNull Throwable t) {

            }
        });
    }

    protected void trackLocation() {
        if (!checkIfGooglePlayEnabled()) {
            return;
        }
        final LocationManager manager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

            } else {


            }
        } else {


        }

    }

    private void buildAlertMessageNoGps() {
        builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Location Services Not Active");
        builder.setMessage("Please enable Location Services and GPS");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                // Show location settings when the user acknowledges the alert dialog
                startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 1);
            }

        });
        alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Toast.makeText(getApplicationContext(), "Disabled", Toast.LENGTH_LONG).show();
            buildAlertMessageNoGps();

        } else {
            Toast.makeText(getApplicationContext(), "Location  On", Toast.LENGTH_LONG).show();
            alertDialog.dismiss();

        }
    }


}



