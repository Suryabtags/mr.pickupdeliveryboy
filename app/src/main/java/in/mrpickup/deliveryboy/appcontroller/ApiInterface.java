package in.mrpickup.deliveryboy.appcontroller;

import java.util.ArrayList;

import in.mrpickup.deliveryboy.latlongSend;
import in.mrpickup.deliveryboy.model.CancelOrder;
import in.mrpickup.deliveryboy.model.EndTrip;
import in.mrpickup.deliveryboy.model.FcmToken;
import in.mrpickup.deliveryboy.model.ProfileView;
import in.mrpickup.deliveryboy.model.Rates;
import in.mrpickup.deliveryboy.model.StartTrip;
import in.mrpickup.deliveryboy.model.UpdateLeave;
import in.mrpickup.deliveryboy.model.UpdateLeaveResponse;
import in.mrpickup.deliveryboy.model.WaitingTime;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Url;

/**
 * Created by admin on 10/26/2017.
 */

public interface ApiInterface {


    @PUT("employee/delivery_boy/update_location/")
    Call<latlongSend> getlatlong(@Header("Authorization") String token, @Body latlongSend forgotsend);

    @GET("customers/")
    Call<ProfileView> getProfile(@Header("Authorization") String token);


    @Multipart
    @PUT("customers/update_profile/")
    Call<ProfileView> profileedit(@Header("Authorization") String token,
                                  @Part("first_name") RequestBody firstname,
                                  @Part("email") RequestBody email,
                                  @Part("username") RequestBody username,
                                  @Part("useraddress") RequestBody jsonObject,
                                  @Part MultipartBody.Part profile_pic,
                                  @Part("dob") RequestBody dob);

    @Multipart
    @PUT("customers/update_profile/")
    Call<ProfileView> profileedit1(@Header("Authorization") String token,
                                   @Part("first_name") RequestBody firstname,
                                   @Part("email") RequestBody email,
                                   @Part("username") RequestBody username,
                                   @Part("useraddress") RequestBody jsonObject,
                                   @Part("dob") RequestBody dob);


    @PUT
    Call<Void> getCancelOrder(@Url String url, @Header("Authorization") String token, @Body CancelOrder cancelOrder);

    @PUT("customers/update_device_token/")
    Call<Void> updateFcm(@Header("Authorization") String token, @Body FcmToken fcmToken);

    @GET("rates/")
    Call<ArrayList<Rates>> getRates();


    @Multipart
    @POST("employee/delivery_boy/start_trip/")
    Call<StartTrip> startTrip(@Header("Authorization") String token,
                              @Part("otp") RequestBody otp,
                              @Part("weight") RequestBody weight,
                              @Part("is_first") RequestBody is_first,
                              @Part("latitude") RequestBody latitude,
                              @Part("longitude") RequestBody longitude,
                              @Part("order") RequestBody order,
                              @Part MultipartBody.Part images);

    @Multipart
    @POST("employee/delivery_boy/end_trip/")
    Call<EndTrip> endTrip(@Header("Authorization") String token,
                          @Part("order") RequestBody order,
                          @Part("is_first") RequestBody is_first,
                          @Part("latitude") RequestBody latitude,
                          @Part("longitude") RequestBody longitude,
                          @Part("otp") RequestBody otp,
                          @Part("waiting_time") RequestBody waiting_time,
                          @Part MultipartBody.Part images);

    @POST
    Call<WaitingTime> waitingTime(@Header("Authorization") String token,
                                  @Url String url,
                                  @Body WaitingTime waitingTime);

    @PUT("employee/delivery_boy/update_location/")
    Call<UpdateLeaveResponse> updateLeave(@Header("Authorization") String token, @Body UpdateLeave updateLeave);

    @PUT
    Call<WaitingTime> waitingTime1(@Header("Authorization") String token,
                                   @Url String url,
                                   @Body WaitingTime waitingTime);

}
