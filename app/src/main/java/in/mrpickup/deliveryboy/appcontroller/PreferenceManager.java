package in.mrpickup.deliveryboy.appcontroller;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

/**
 * Created by sase on 6/24/2017
 */

public class PreferenceManager {
    private static final String Token = "token";
    private static final String PREF_LOCATION = "location";
    private static final String PREF_EXTRAS = "extras";
    private static final String PREF_USER = "user";
    private static final String User_name = "username";
    private static final String FCM_TOKEN = "device_token";
    private static final String TOKEN_UPLOADED = "token_uploaded";
    private static final String START_WAIT = "start";
    private static final String START_WAITINGTIME = "startwaiting";
    private static final String ORDER_ID = "order_id";
    private static final String OTP = "otp";
    private static final String POLYLINE = "polyline";
    private static final String ORDERWAIT = "orderwait";
    private SharedPreferences mPreferencesLocation, mPreferencesExtras, mPreferencesUser;

    public PreferenceManager(Context context) {
        mPreferencesLocation = context.getSharedPreferences(PREF_LOCATION, Context.MODE_PRIVATE);
        mPreferencesExtras = context.getSharedPreferences(PREF_EXTRAS, Context.MODE_PRIVATE);
        mPreferencesUser = context.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
    }


    private String encode(String data) {
        return Base64.encodeToString(data.getBytes(), Base64.NO_WRAP);
    }

    public int getOrderId() {
        return mPreferencesUser.getInt(encode(ORDER_ID), -1);
    }

    public void setOrderId(int id) {
        mPreferencesUser.edit().putInt(encode(ORDER_ID), id).apply();
    }


    public int getOrderWait() {
        return mPreferencesUser.getInt(encode(ORDERWAIT), -1);
    }

    public void setOrderWait(int id) {
        mPreferencesUser.edit().putInt(encode(ORDERWAIT), id).apply();
    }

    public String getToken() {

        return mPreferencesUser.getString(encode(Token), null);
    }

    public void setToken(String token) {
        mPreferencesUser.edit().putString(encode(Token), token).apply();
    }


    private String getName() {
        return mPreferencesUser.getString(encode(User_name), null);
    }

    public void setName(String username) {
        mPreferencesUser.edit().putString(encode(User_name), username).apply();
    }

    public boolean isTokenUploaded() {
        return mPreferencesExtras.getBoolean(encode(TOKEN_UPLOADED), false);
    }

    public void setTokenUploaded(boolean tokenUploaded) {
        mPreferencesExtras.edit().putBoolean(encode(TOKEN_UPLOADED), tokenUploaded).apply();
    }

    public String getFcmToken() {
        return mPreferencesExtras.getString(encode(FCM_TOKEN), null);
    }

    void setFcmToken(String fcmToken) {
        mPreferencesExtras.edit().putString(encode(FCM_TOKEN), fcmToken).apply();
    }

    public void clearUser() {
        setTokenUploaded(false);
        mPreferencesUser.edit().clear().apply();
    }

    public String getStartWait() {
        return mPreferencesExtras.getString(encode(START_WAIT), null);
    }

    public void setStartWait(String Startwait) {
        mPreferencesExtras.edit().putString(encode(START_WAIT), Startwait).apply();
    }

    public String getStartWaitingtime() {
        return mPreferencesExtras.getString(encode(START_WAITINGTIME), null);
    }

    public void setStartWaitingTime(String StartwaitingTime) {
        mPreferencesExtras.edit().putString(encode(START_WAITINGTIME), StartwaitingTime).apply();
    }

    public String getotp() {
        return mPreferencesExtras.getString(encode(OTP), null);
    }

    public void setOtp(String otp) {
        mPreferencesExtras.edit().putString(encode(OTP), otp).apply();
    }

    public String getPolyline() {
        return mPreferencesExtras.getString(encode(POLYLINE), null);
    }

    public void setPolyline(String poly) {
        mPreferencesExtras.edit().putString(encode(POLYLINE), poly).apply();
    }
}
