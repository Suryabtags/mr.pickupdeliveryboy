package in.mrpickup.deliveryboy.appcontroller;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;


public class FirebaseService extends FirebaseInstanceIdService {


    @Override
    public void onTokenRefresh() {
        String token = FirebaseInstanceId.getInstance().getToken();

        Log.e("myfcmtoken", "" + token);

        storeToken(token);
    }

    private void storeToken(String token) {

        PreferenceManager myPreference = new PreferenceManager(this);
        myPreference.setFcmToken(token);
        myPreference.setTokenUploaded(false);
    }
}

