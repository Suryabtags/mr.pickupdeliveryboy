package in.mrpickup.deliveryboy.appcontroller;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

import in.mrpickup.deliveryboy.Orders;
import in.mrpickup.deliveryboy.R;

/**
 * Created by gokul on 6/17/2017
 */


public class FireBaseNotification extends FirebaseMessagingService {
    private static final int REQUEST_NOTIFICATION = 4;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        JSONObject jsonObject = new JSONObject(remoteMessage.getData());
        String ticker = null;
        String title = null;
        String description = null;
        PreferenceManager preferenceManager = new PreferenceManager(this);

        try {
            ticker = jsonObject.getString(Api.KEY_TICKER);
            title = jsonObject.getString(Api.KEY_TITLE);
            description = jsonObject.getString(Api.KEY_DESCRIPTION);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        Log.e("fcm-notifications", "" + preferenceManager.getToken());
        if (preferenceManager.getToken() != null) {
            displayNotification(ticker, title, description);
            Log.d("ticker-token", "" + ticker);
            Log.d("ticker-token", "" + title);
            Log.d("description-token", "" + description);
        }
    }

    private void displayNotification(String ticker, String title, String description) {

        Intent intent = new Intent(this, Orders.class);
        PendingIntent pendingIntentContent = PendingIntent.getActivity(this, REQUEST_NOTIFICATION,
                intent, PendingIntent.FLAG_ONE_SHOT);

        Bitmap bitmapLargeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.finallogo);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setTicker(ticker)
                .setContentTitle(title)
                .setContentText(description)
                .setContentIntent(pendingIntentContent)
                .setSmallIcon(R.drawable.finallogo)
                .setLargeIcon(bitmapLargeIcon)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(description))
                .setSound(defaultSoundUri)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_MAX)
                .setDefaults(Notification.DEFAULT_ALL);


        Random random = new Random();
        int notificationId = random.nextInt(9999 - 1000) + 1000;

        NotificationManager notificationManager = (NotificationManager) getSystemService(
                Context.NOTIFICATION_SERVICE);
        notificationManager.notify(notificationId, builder.build());

    }
}

