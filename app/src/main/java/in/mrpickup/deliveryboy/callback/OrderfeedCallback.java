package in.mrpickup.deliveryboy.callback;

/**
 * Created by Sase on 09-06-2017
 */

public interface OrderfeedCallback {
    void onItemClick(int position);

    void onGoclick(int position);

    void onPcallclick(int position);

    void onviewInfoclick(int position);

    void onDcalclick(int position);


}
