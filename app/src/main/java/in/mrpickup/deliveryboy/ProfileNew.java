package in.mrpickup.deliveryboy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import de.hdodenhof.circleimageview.CircleImageView;
import in.mrpickup.deliveryboy.appcontroller.ApiClient;
import in.mrpickup.deliveryboy.appcontroller.ApiInterface;
import in.mrpickup.deliveryboy.appcontroller.PreferenceManager;
import in.mrpickup.deliveryboy.helper.NetworkError;
import in.mrpickup.deliveryboy.model.ProfileView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileNew extends AppCompatActivity {


    public ImageView profile_edit, img_back;
    TextView usr_name, usr_phone, usr_mail, edit_dob, edit_Street, edit_City, edit_Area;
    TextView edit_state, edit_Country, edit_Pincode;
    private CircleImageView mImageViewProfile;
    private PreferenceManager mPreference;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        initObjects();

        if (NetworkError.getInstance(this).isOnline()) {
            getProfilemethod();
        } else {
            ToastBuilder.build(this, " No Internet Connection");
        }


    }

    private void initObjects() {
        mImageViewProfile = findViewById(R.id.img_profile);
        profile_edit = findViewById(R.id.profile_edit);
        img_back = findViewById(R.id.img_back);
        usr_name = findViewById(R.id.usr_name);
        usr_phone = findViewById(R.id.usr_phone);
        usr_mail = findViewById(R.id.usr_mail);
        edit_dob = findViewById(R.id.edit_dob);
        edit_Street = findViewById(R.id.edit_Street);
        edit_City = findViewById(R.id.edit_City);
        edit_state = findViewById(R.id.edit_state);
        edit_Country = findViewById(R.id.edit_Country);
        edit_Pincode = findViewById(R.id.edit_Pincode);
        edit_Area = findViewById(R.id.edit_Area);
        mPreference = new PreferenceManager(this);
        mContext = this;
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        profile_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), EditProfile.class);
                startActivity(intent);
            }
        });

    }

    public void getProfilemethod() {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ProfileView> call = apiService.getProfile("Token " + mPreference.getToken());
        call.enqueue(new Callback<ProfileView>() {

            @Override
            public void onResponse(@NonNull Call<ProfileView> call, @NonNull Response<ProfileView> response) {
                ProfileView profileView = response.body();
                if (response.isSuccessful() && profileView != null) {
                    usr_name.setText(profileView.getFirst_name());
                    usr_phone.setText(profileView.getUsername());
                    usr_mail.setText(profileView.getEmail());
                    edit_dob.setText(profileView.getUserprofile().getDob());
                    edit_Street.setText(profileView.getUseraddress().getStreet());
                    edit_City.setText(profileView.getUseraddress().getCity());
                    edit_state.setText(profileView.getUseraddress().getState());
                    edit_Pincode.setText(profileView.getUseraddress().getZipcode());
                    edit_Country.setText(profileView.getUseraddress().getCountry());
                    edit_Area.setText(profileView.getUseraddress().getArea());
                    String imageloader = profileView.getUserprofile().getProfile_pic();
                    if (imageloader != null) {
                        Glide.with(mContext).load(imageloader)
                                .apply(new RequestOptions().placeholder(R.drawable.man).error(R.drawable.man))
                                .into(mImageViewProfile);
                    }


                }
            }

            @Override
            public void onFailure(@NonNull Call<ProfileView> call, @NonNull Throwable t) {
                Log.e("failuretokenprofile", "" + t.getMessage());
            }
        });
    }

    @Override
    protected void onResume() {
        getProfilemethod();
        super.onResume();
    }
}
