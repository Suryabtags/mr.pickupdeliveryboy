package in.mrpickup.deliveryboy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import in.mrpickup.deliveryboy.Adapter.ProductListAdapter;
import in.mrpickup.deliveryboy.appcontroller.Constant;
import in.mrpickup.deliveryboy.appcontroller.PreferenceManager;
import in.mrpickup.deliveryboy.model.Feed;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class OrderDetailsActivity extends AppCompatActivity implements View.OnClickListener {


    public TextView orderId, pickAddr, dropAddr, date, name,
            category, delType, mStore, mPayment, txtOrdertype, txtWeight, txt_kilometer, txt_description, txt_deliveryname;
    public String pickMob, dropLoc,
            pickAddr1, type, delName, poly, approx_dist,
            approx_time, delMob, otp, name1,
            date11, Storename, mPaymentType, mOrderType, mWeight, mDescription, mDeliveryName, mOrderShow;
    public PreferenceManager mPreference;
    Button mContinue;
    int Oid;
    int status;
    ImageView img_back;
    ArrayList<Feed> mOrderArray = new ArrayList<>();
    ArrayList<Feed> mOrders = new ArrayList<>();
    ProductListAdapter productEditListAdapter;
    LinearLayout mtxt_kilometrlayout;
    private Context mContext;
    private boolean pickType, waitTime;
    private double pickLat, pickLng, droplat, droplng;
    private LinearLayout mStoreLayouts, mDescriptionLayout;
    private RecyclerView mProductsListview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myorder_details);
        processBundle();
        initObjects();
        initCallbacks();
    }

    private void initObjects() {

        mContext = this;
        orderId = findViewById(R.id.order_id);
        img_back = findViewById(R.id.img_back);
        pickAddr = findViewById(R.id.pickupaddress);
        dropAddr = findViewById(R.id.dropaddress);
        date = findViewById(R.id.created_date);
        name = findViewById(R.id.name);
        txt_description = findViewById(R.id.txt_descr);
        txt_deliveryname = findViewById(R.id.del_name);
        delType = findViewById(R.id.type);
        mContinue = findViewById(R.id.continue11);
        mStore = findViewById(R.id.store);
        mPayment = findViewById(R.id.payment_type);
        mStoreLayouts = findViewById(R.id.storelayout);
        txtOrdertype = findViewById(R.id.txt_orderType);
        txt_kilometer = findViewById(R.id.txt_Kilometer);
        mtxt_kilometrlayout = findViewById(R.id.txt_kilometrlayout);
        mDescriptionLayout = findViewById(R.id.descriptionLayout);
        txtWeight = findViewById(R.id.weight);
        mPreference = new PreferenceManager(mContext);
        mProductsListview = findViewById(R.id.products_listview);
        txt_kilometer.setVisibility(View.GONE);
        mtxt_kilometrlayout.setVisibility(View.GONE);
        initSetText();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    private void initSetText() {
        String mWeight1 = mWeight;
//        int index = mWeight1.indexOf(".");
//        String mWeight11 = mWeight1.substring(0, index);
        txtWeight.setText(mWeight1 + " Kg");
        orderId.setText(String.valueOf(mOrderShow));
        pickAddr.setText(pickAddr1);
        dropAddr.setText(dropLoc);
        date.setText(date11);
        name.setText(name1);
        mPayment.setText(mPaymentType);
        txt_deliveryname.setText(mDeliveryName);


        if (!mDescription.isEmpty()) {
            txt_description.setText(mDescription);
            mDescriptionLayout.setVisibility(View.VISIBLE);
        } else {
            mDescriptionLayout.setVisibility(View.GONE);
        }

        if (pickType) {
            delType.setText("PickUp & Delivery");
            mStoreLayouts.setVisibility(View.GONE);

        } else {
            delType.setText("Buy & Delivery");
            mStoreLayouts.setVisibility(View.VISIBLE);
            mStore.setText(Storename);
        }
        if (mOrderType.equals("OR")) {
            txtOrdertype.setText("Ordinary");
        } else {
            txtOrdertype.setText("Urgent");
        }
        if (mPaymentType.equals("CD")) {
            mPayment.setText("Cash On Delivery");
        } else {
            mPayment.setText("Online");
        }


        mProducts();
    }

    private void initCallbacks() {
        mContinue.setOnClickListener(this);
        img_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v == mContinue) {
            Intent intent = new Intent(getBaseContext(), MapActivity.class);
            intent.putExtra("SOURCE_LAT", pickLat);
            intent.putExtra("SOURCE_LNG", pickLng);
            intent.putExtra("NAME", name1);
            intent.putExtra("PICK_LOC", pickAddr1);
            intent.putExtra("DROP_LOC", dropLoc);
            intent.putExtra("ORDER_ID", Oid);
            intent.putExtra("PICK_MOB", pickMob);
            intent.putExtra("DELIVERY_MOB", delMob);
            intent.putExtra("PICK_TYPE", pickType);
            intent.putExtra("STATUS", status);
            intent.putExtra("DELIVER_NAME", delName);
            intent.putExtra("DEL_LAT", droplat);
            intent.putExtra("DEL_LNG", droplng);
            intent.putExtra("Date", date11);
            intent.putExtra("ordershow", mOrderShow);
            startActivity(intent);
            finish();

        } else if (v == img_back) {
            onBackPressed();
        }

    }

    private void mProducts() {
        if (mOrders != null) {
            mProductsListview.setVisibility(View.VISIBLE);
            productEditListAdapter = new ProductListAdapter(mContext, mOrders);
            mProductsListview.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, true));
            mProductsListview.setHasFixedSize(true);
            mProductsListview.setScrollBarSize(0);
            mProductsListview.setAdapter(productEditListAdapter);
            productEditListAdapter.notifyDataSetChanged();

        }
    }

    private void processBundle() {
        name1 = getIntent().getExtras().getString("NAME");
        pickAddr1 = getIntent().getExtras().getString("PICK_LOC");
        Oid = getIntent().getExtras().getInt("ORDER_ID");
        pickMob = getIntent().getExtras().getString("PICK_MOB");
        delMob = getIntent().getExtras().getString("DELIVERY_MOB");
        dropLoc = getIntent().getExtras().getString("DROP_LOC");
        pickType = getIntent().getExtras().getBoolean("PICK_TYPE");
        pickLat = getIntent().getExtras().getDouble("SOURCE_LAT");
        pickLng = getIntent().getExtras().getDouble("SOURCE_LNG");
        status = getIntent().getExtras().getInt("STATUS");
        droplat = getIntent().getExtras().getDouble("DEL_LAT");
        droplng = getIntent().getExtras().getDouble("DEL_LNG");
        date11 = getIntent().getExtras().getString("Date");
        delName = getIntent().getExtras().getString("DELIVER_NAME");
        Storename = getIntent().getExtras().getString("StoreName");
        mPaymentType = getIntent().getExtras().getString("PaymentType");
        mOrderType = getIntent().getExtras().getString("ordertype");
        mWeight = getIntent().getExtras().getString("weight");
        mDescription = getIntent().getExtras().getString("Description");
        mDeliveryName = getIntent().getExtras().getString("DELNAME");
        mOrderShow = getIntent().getExtras().getString("ordershow");
        mOrderArray = getIntent().getParcelableArrayListExtra(Constant.TRIP_ARRAY);

        for (int i = 0; i < mOrderArray.size(); i++) {

            if (Oid == mOrderArray.get(i).getmId()) {
                mOrders.add(mOrderArray.get(i));
                Log.e("djfgjdf", "" + mOrders.size());

            }
        }
    }


}
