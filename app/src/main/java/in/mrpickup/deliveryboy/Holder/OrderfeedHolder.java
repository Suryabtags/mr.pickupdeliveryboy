package in.mrpickup.deliveryboy.Holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.mrpickup.deliveryboy.R;
import in.mrpickup.deliveryboy.callback.OrderfeedCallback;

/**
 * Created by sase on 09-06-2017
 */

public class OrderfeedHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


    public TextView orderId, pickAddr, dropAddr, date, name, category, delType, payType, mTripStatus, mTripOrderInfo, mTripType;
    public ImageView cancel_order;
    private ImageView dcal, pcal;
    private OrderfeedCallback mCallback;

    public OrderfeedHolder(View itemView, OrderfeedCallback callback) {
        super(itemView);
        orderId = itemView.findViewById(R.id.order_id);
        pickAddr = itemView.findViewById(R.id.pickupaddress);
        dropAddr = itemView.findViewById(R.id.dropaddress);
        date = itemView.findViewById(R.id.created_date);
        name = itemView.findViewById(R.id.name);
        category = itemView.findViewById(R.id.category);
        delType = itemView.findViewById(R.id.delivery_type);
        payType = itemView.findViewById(R.id.payment_type);
        cancel_order = itemView.findViewById(R.id.cancel_order);
        mTripStatus = itemView.findViewById(R.id.trip_status);
        mTripOrderInfo = itemView.findViewById(R.id.view_Info);
        mTripType = itemView.findViewById(R.id.txt_trip_type);
        mCallback = callback;
        itemView.setOnClickListener(this);
        mTripOrderInfo.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        if (v == dcal) {
            mCallback.onDcalclick(getLayoutPosition());
        } else if (v == pcal) {
            mCallback.onPcallclick(getLayoutPosition());
        } else if (v == itemView) {
            mCallback.onItemClick(getLayoutPosition());
        } else if (v == mTripOrderInfo) {
            mCallback.onviewInfoclick(getLayoutPosition());
        }
    }
}