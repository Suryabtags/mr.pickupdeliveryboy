package in.mrpickup.deliveryboy.Holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import in.mrpickup.deliveryboy.R;


public class AddProductHolder extends RecyclerView.ViewHolder {

    public TextView mCategory, mSubCategory, mQuantity;


    public AddProductHolder(View itemView) {
        super(itemView);
        mCategory = itemView.findViewById(R.id.category);
        mSubCategory = itemView.findViewById(R.id.subcategory);
        mQuantity = itemView.findViewById(R.id.quantity);

    }


}
