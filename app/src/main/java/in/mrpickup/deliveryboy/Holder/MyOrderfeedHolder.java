package in.mrpickup.deliveryboy.Holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import in.mrpickup.deliveryboy.R;
import in.mrpickup.deliveryboy.callback.OrderfeedCallback;


public class MyOrderfeedHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


    public TextView orderId, pickAddr, dropAddr, date, name, category,
            delType, payType, mTripStatus, ViewComments, mPayment;
    public ImageView cancel_order;
    public RatingBar mRatingBar;
    private ImageView dcal, pcal;
    private OrderfeedCallback mCallback;

    public MyOrderfeedHolder(View itemView, OrderfeedCallback callback) {
        super(itemView);
        orderId = itemView.findViewById(R.id.order_id);
        pickAddr = itemView.findViewById(R.id.pickupaddress);
        dropAddr = itemView.findViewById(R.id.dropaddress);
        date = itemView.findViewById(R.id.created_date);
        name = itemView.findViewById(R.id.name);
        category = itemView.findViewById(R.id.category);
        delType = itemView.findViewById(R.id.delivery_type);
        payType = itemView.findViewById(R.id.payment_type);
        cancel_order = itemView.findViewById(R.id.cancel_order);
        mTripStatus = itemView.findViewById(R.id.trip_status);
        ViewComments = itemView.findViewById(R.id.ViewComments);
        mRatingBar = itemView.findViewById(R.id.rating);
        mPayment = itemView.findViewById(R.id.payment);

        mCallback = callback;
        itemView.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        if (v == dcal) {
            mCallback.onDcalclick(getAdapterPosition());
        } else if (v == pcal) {
            mCallback.onPcallclick(getAdapterPosition());
        } else if (v == itemView) {
            mCallback.onItemClick(getAdapterPosition());
        }

    }
}